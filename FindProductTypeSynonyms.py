#!/usr/bin/python
# -*- coding: utf-8

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, OperationFailure
from CleaningHelper import remove_brand_and_product_type
from CleaningHelper import find_potential_product_type
from CleaningHelper import remove_elements_by_threshold
from ProductTokenizer import ProductTokenizer
from collections import OrderedDict
from operator import itemgetter


# Вспомогательный класс, используется для хранения информации
# о товаре, в виде, удобном для дальнейшей классификации
class ProductToClassify:

    def __init__(self, _id, _brand, _main_feature, _original_text):
        self.id = _id
        self.brand = _brand
        self.main_feature = _main_feature
        self.original_text = _original_text
        self.additional_features = None

    def copy_additional_features(self, _additional_features):

        if _additional_features is not None and len(_additional_features) > 0:
            self.additional_features = _additional_features

    # Функция реализует тривиальные правила эквивалентности товаров
    def is_equal(self, another_product):

        # CONDITION 1: Бренды, под которыми выпущены товары, должны быть одинаковыми
        if another_product.brand != self.brand:
            return False

        # Если дополнительных свойств нет то используем точное сравнение
        if self.additional_features is None or another_product.additional_features is None:

            if self.additional_features is None and another_product.additional_features is None:
                if self.main_feature == another_product.main_feature:
                    return True

            return False

        # CONDITION 2: Основное свойство одного товара должно соответствовать основному
        # свойству другого товара, или находится в списке дополнительных свойств
        if self.main_feature != another_product.main_feature:
            if self.main_feature not in another_product.additional_features:
                if another_product.main_feature not in self.additional_features:
                    return False

        # CONDITION 3: Определяем процент пересечения дополнительных свойств товара
        # друг с другом
        set_one = set(self.additional_features)
        set_one.add(self.main_feature)

        set_two = set(another_product.additional_features)
        set_two.add(another_product.main_feature)

        common_elements_count = len(list(set_one & set_two))
        max_element_count = max(len(set_one), len(set_two))

        if common_elements_count / max_element_count > 0.74:
            return True

        return False


def print_product_types_by_articles(db_connection):

    playsets_collection = db_connection.scrape.playsets.find({"brand": "Pampers"})
    products_count = playsets_collection.count()
    print('Товаров с указанным брендом: {}'.format(products_count))

    articles_to_product_types = {}

    # Группируем товары по артикулу
    processed_count = 0
    for element in playsets_collection:

        # Игнорируем товары, у которых отсутствует артикул товара продавца
        if element.get('article_man') is None or element.get('name') is None:
            continue

        product_code = element['article_man']
        if product_code not in articles_to_product_types:
            articles_to_product_types[product_code] = []

        articles_to_product_types[product_code].append(element['name'])

        processed_count += 1

    for article in list(articles_to_product_types.keys()):
        print('{} -> {}'.format(article, articles_to_product_types[article]))

    print('Обработанных товаров: {}'.format(processed_count))


# Функция осуществляет поиск типов товаров по хлебным крошкам
def find_product_types_by_breadcrumb(playsets_collection, detailed_log=False):

    product_types = []

    for element in playsets_collection.rewind():

        if element.get('breadcrumb') is None or element.get('name') is None or element.get('brand') is None:
            continue

        _product_type = find_potential_product_type(element['name'], element['breadcrumb'])
        if _product_type is not None and len(_product_type) > 0:
            if _product_type not in product_types:
                product_types.append(_product_type)

    if detailed_log:
        print('Найденные, по "хлебным крошкам", типы товаров: {}'.format(product_types))

    return product_types


# Функция осуществляет выделения явно указанных типов товаров
def select_specified_product_types(playsets_collection, detailed_log=False):

    product_types = []

    for element in playsets_collection.rewind():

        if element.get('producttype') is not None:

            _product_type = element['producttype'].upper()
            if len(_product_type) > 0:
                if _product_type not in product_types:
                    product_types.append(_product_type)

    if detailed_log:
        print('Явно указанные типы товаров: {}'.format(product_types))

    return product_types


# Функция осуществляет поиск типов товаров по бренду товара
def find_product_types_by_position(playsets_collection, detailed_log=False):

    product_types = []

    for element in playsets_collection.rewind():

        if element.get('name') is None or element.get('brand') is None:
            continue

        _upper_name = element['name'].upper()
        _upper_brand = element['brand'].upper()

        pos = _upper_name.find(_upper_brand)
        if pos >= 0:

            _product_type = _upper_name[:pos].strip()
            if len(_product_type) > 0:
                if _product_type not in product_types:
                    product_types.append(_product_type)

    if detailed_log:
        print('Типы товаров найденные по бренду: {}'.format(product_types))

    return product_types


def find_models(collection, restricted_models, product_types, prohibited_words):

    # Для каждого товара, из которого исключен бренд и тип товара выделяем
    # потенциальное название модели.

    # Маловероятно, что название модели состоит более, чем из четырёх слов
    model_variants = {}

    for element in collection:

        if element.get('name') is None or element.get('brand') is None:
            continue

        # Не учитываем продукт, если у него чётко выделяется ранее найденная модель
        if restricted_models is not None:
            ignore_product = False
            for restriction in restricted_models:
                if element['name'].upper().find(restriction) >= 0:
                    ignore_product = True
                    break

            if ignore_product:
                continue

        # Удаляем из названия товара бренд и название товара
        _name = remove_brand_and_product_type(element['name'], element['brand'], product_types).strip()

        # Пересобираем название товара таким образом, чтобы убрать пробелы
        # вокруг амперсанда. Например: SLEEP & PLAY => SLEEP&PLAY

        # TODO: заменить на два replace

        do_add_space = False
        joined_words = ''
        for _word in _name.split(' '):

            _stripped_word = _word.strip()
            if _stripped_word == '&':

                joined_words += _stripped_word
                do_add_space = False

            else:

                if do_add_space:
                    joined_words += ' '

                joined_words += _stripped_word

                do_add_space = True

        _name = joined_words

        # Снова разбираем название на слова, но теперь "SLEEP&PLAY" идёт
        # без пробелов вокруг символа-амперсанда
        count = 0
        joined_words = ''
        for _word in _name.split(' '):

            # Если мы дошли до запрещённого слова, считаем, что название
            # модели уже завершилось
            if _word in prohibited_words:
                break

            if count > 0:
                joined_words += ' '

            joined_words += _word

            if model_variants.get(joined_words) is None:
                model_variants[joined_words] = 1
            else:
                model_variants[joined_words] += 1

            count += 1
            if count > 3:
                break

    # После того, как для всех названий сформированы варианты модели,
    # сортируем их по количеству упоминаний
    return remove_elements_by_threshold(model_variants)


# Функция определяет товарные знаки, входящие в состав названия товаров
def find_product_groups_by_comma(collection):

    candidates = {}

    for element in collection:

        if element.get('name') is None or element.get('brand') is None:
            continue

        _candidates = element['name'].split(',')

        for index, text in enumerate(_candidates):
            if index == 0:
                continue

            if candidates.get(text) is None:
                candidates[text] = 1
            else:
                candidates[text] += 1

    # После того, как для всех подсчитана частота упоминания блоков текста,
    # идущей после запятой сортируем их по количеству упоминаний
    return remove_elements_by_threshold(candidates)


# Функция осуществляет попытку разделить текст в кавычках, в названиях товаров на
# названия серий и названия моделей
def find_series_and_model_candidates(collection):

    candidates = {}

    model_candidates = []

    for element in collection:

        if element.get('name') is None or element.get('brand') is None:
            continue

        _candidates = element['name'].split('"')

        for index, text in enumerate(_candidates):

            # Игнорируем чётные элементы, т.к. текст внутри кавычек - это чётные элементы.
            # Для примера:
            #   Набор мебели "Гриль и стул" из серии "Отдых на природе", Barbie
            if index % 2 == 0:
                continue

            # Если в названии продукта есть несколько подстрок с кавычками,
            # то первый считаем названием набора, а второй - серии
            if len(_candidates) < 4 or index > 1:

                if candidates.get(text) is None:
                    candidates[text] = 1
                else:
                    candidates[text] += 1
            else:
                model_candidates.append(text)

    # После того, как для всех подсчитана частота упоминания блоков
    # текста в кавычках, сортируем их по количеству упоминаний и
    # удаляем те, которые редко встречаются
    series_candidates = remove_elements_by_threshold(candidates)

    # Возвращаем как названия серий товаров, так и потенциальные названия моделей
    return series_candidates, model_candidates


def print_product_types_by_names(db_connection, brands_list, algorithm, deny_variations_in_model=False):
    """
    :param db_connection: connector к базе данных
    :param list brands_list: список брендов (используется для отбора данных из базу
    :param algorithm: алгоритм чистки и группировки данных
    :param deny_variations_in_model: True - отключить выделение вариаций из названия товаров
    """

    # TODO: глобальный словарь товарных знаков нужно хранить в базе данных,
    # т.е. сделать настраиваемым
    global_trademarks = ['DISNEY PRINCESS', 'DISNEY FROZEN', 'ЗВЁЗДНЫЕ ВОЙНЫ', 'STAR WARS',
                         'THOMAS&FRIENDS', 'ТОМАС И ЕГО ДРУЗЬЯ', 'DC COMICS', 'MINECRAFT']

    # Динамически составляем список условий, в зависимости от количества указанных брендов
    if len(brands_list) == 1:
        condition = {"brand": brands_list[0]}
    else:

        # В MongoDB команда проверки нескольких условий выглядит следующим образом:
        #   { $or: [ { <expression1> }, { <expression2> }, ... , { <expressionN> } ] }
        # Например:
        #   db.inventory.find( { $or: [ { quantity: { $lt: 20 } }, { price: 10 } ] } )
        #
        # Ссылка: https://docs.mongodb.com/manual/reference/operator/query/or/

        elements = []
        for _brand in brands_list:
            elements.append({"brand": _brand})

        condition = {"$or": elements}

    # Отбираем элементы с разным написанием бренда из базы данных
    playsets_collection = db_connection.scrape.playsets.find(condition)
    products_count = playsets_collection.count()
    print('Товаров с указанным брендом: {}'.format(products_count))

    if algorithm == 1:

        # STEP 1:
        # Собираем набор полный набор возможных типов товаров

        # Вводим ограничение на длину типа товара, т.к. в исходных данных часто
        # встречается мусор
        min_product_type_length = 3

        # Опасность breadcrumb состоит в том, что они могут хранить в себе
        # названия товарных знаков
        product_types = find_product_types_by_breadcrumb(playsets_collection)

        # Практически тоже самое можно сказать и о значении поля "producttype" -
        # в этом поле может быть абсолютно всё, что угодно
        for _product_type in select_specified_product_types(playsets_collection):
            _ptype = _product_type.strip()
            if len(_ptype) >= min_product_type_length:
                if _ptype not in product_types:
                    product_types.append(_ptype)

        for _product_type in find_product_types_by_position(playsets_collection):
            _ptype = _product_type.strip()
            if len(_ptype) >= min_product_type_length:
                if _ptype not in product_types:
                    product_types.append(_ptype)

        # Сортируем список типов товаров по длине. Короткие строки находятся в конце списка.
        product_types.sort(key=len, reverse=True)  # ToDo: актуально для Pampers

        prohibited_words = ['РАЗМЕР', 'КГ']

        for element in playsets_collection.rewind():    # rewind() - позволяет вернуть курсор на начало коллекции

            # В список исключений добавляем названия свойств товара и размерные единицы
            if element.get('properties') is not None:

                for _property in element['properties']:

                    if _property.get('name'):
                        _value = _property['name'].upper()
                        if _value not in prohibited_words:
                            prohibited_words.append(_value)

                    if _property.get('unit'):
                        _value = _property['unit'].upper()
                        if _value not in prohibited_words:
                            prohibited_words.append(_value)

                # Экспериментально, включаем вариации в список исключений
                if deny_variations_in_model:

                    if element.get('name') is not None:

                        _name = element['name']
                        _begin_pos = _name.find('(')
                        _end_pos = _name.find(')')

                        if 0 <= _begin_pos < _end_pos:

                            _variation = _name[_begin_pos + 1:_end_pos].strip().upper()
                            _pos = _variation.find(' ')
                            if _pos >= 0:
                                _variation = _variation[:_pos]

                            if _variation not in prohibited_words:
                                prohibited_words.append(_variation)

        # STEP 3:
        # Выводим результат - найденный список моделей

        print()

        # В первом алгоритме, бренд и тип товара находятся в самом начале
        # полного названия товара и легко отсекаются
        main_models = find_models(playsets_collection.rewind(), None, product_types, prohibited_words)
        print('Извлечённые названия моделей...')
        for name in main_models:
            print('  {}'.format(name))

        model_candidates = find_models(playsets_collection.rewind(), main_models, product_types, prohibited_words)
        print('Дополнительные кандидаты в названия моделей моделей...')
        for name in model_candidates:
            print('  {}'.format(name))

    if algorithm == 2:

        '''
        # Во втором алгоритме, считаем, что бренд и тип товара находятся где угодно.
        # В этой алгоритме формируются два списка элементов, первый из которых хранит
        # либо модель, либо тип товара - эти данные являются обязательными для совпадения.
        # Во втором списке идут названия серий, дополнительные бренды, и т.д. - их
        # совпадение является важным, но списки двух одинаковых товаров могут не совпадать
        # в полной мере

        # Сначала выделяем тексты, идущие после запятой
        product_groups_candidates = find_product_groups_by_comma(playsets_collection.rewind())

        # Если в названии товара содержаться запятые, то текст после них, считаем
        # названиями серий товаров
        series = []

        print('Извлечённые названия серий товаров...')
        for _product_group in product_groups_candidates:
            _pg_upper = _product_group.upper().strip()
            if _pg_upper not in series:
                series.append(_pg_upper)
                print('  {}'.format(_pg_upper))

        # Выделяем потенциальные названия товаров и серий товаров, которые могут
        # быть указаны внутри кавычек
        series_candidates, model_candidates = find_series_and_model_candidates(playsets_collection.rewind())

        print()
        print('Дополнительно извлечённые названия серий товаров...')
        for _name in series_candidates:
            _name_upper = _name.upper().strip()
            if _name_upper not in series:
                series.append(_name_upper)
                print('  {}'.format(_name_upper))

        print()
        print('Дополнительно извлечённые названия названия моделей товаров...')
        for _name in model_candidates:
            _name_upper = _name.upper().strip()
            if _name_upper not in product_types:
                product_types.append(_name_upper)
                print('  {}'.format(_name_upper))
        '''

        # ToDo: нужно было бы ещё выполнить и поиск синонимов, они помогают в группировке

        _products = []

        for element in playsets_collection:

            if element.get('name') is None:
                continue

            # Осуществляем разбор строки текста на отдельные токены
            tokenizer = ProductTokenizer(brands_list)
            tokenizer.tokenize_trivial(element)
            _products.append(tokenizer)

        # После обработки данных токенайзером, пытаемся выделить вспомогательные словари.
        product_types_list, models_list, trademarks_list = _collect_identification_lists(_products)

        for _t in global_trademarks:
            if _t not in trademarks_list:
                trademarks_list.append(_t)

        print()
        print('Выделенные типы товаров:')
        for _item in product_types_list:
            print('  {}'.format(_item))

        print()
        print('Выделенные названия моделей:')
        for _item in models_list:
            print('  {}'.format(_item))

        print()
        print('Выделенные торговые марки и вариации:')
        for _item in trademarks_list:
            print('  {}'.format(_item))

        print('----------------')
        for _p in _products:

            # Вторая стадия - выделяем элементы, используя справочники
            _p.correct_by_dictionaries(product_types_list, models_list, trademarks_list)

            # Третья стадия - уточняем типы, используя эвристические схемы
            _p.finalize()
            print(_p)

            '''
            ...

            # STEP 4: В этой точке у нас есть точный бренд (_brand),
            # текст до бренда (вероятно, тип товара), название товара
            # без бренда и типа, а также два замечательных списка:
            # product_types - типы товаров и модели
            # series - серии товаров и модели
            #
            # Далее мы формируем два списка на основании названия товара:
            # обязательные признаки - тип товара и название модели
            # дополнительные признаки - названия серий, торговые марки, и т.д
            _main_features = []
            _additional_features = []

            # Текст идущий до бренда (если он есть), считаем названием товара
            if _before_brand is not None:
                _main_features.append(_before_brand)

            # Ищем в названии товара, как типы товара с названиями моделей,
            # так и серии и сохраняем из в различных списках
            _right_pos = len(_product_name)

            for _p_type in product_types:
                if _p_type in _product_name:
                    _main_features.append(_p_type)
                    pos = _product_name.find(_p_type)
                    if pos < _right_pos:
                        _right_pos = pos

            for _p_type in series:
                if _p_type in _product_name:
                    _additional_features.append(_p_type)
                    pos = _product_name.find(_p_type)
                    if pos < _right_pos:
                        _right_pos = pos

            if _right_pos > 0:
                _product_name = _product_name[:_right_pos]
            else:

                # Отрабатываем особый случай, когда название товара
                # начинается с альтернативного бренда, например:
                #   Ever After High Кукла Сладкая правда Блонди Локс
                for _p_type in series:
                    if _product_name.find(_p_type) == 0:
                        _product_name = _product_name[len(_p_type):]
                        break

            ...
            
            # Если после обработки строки с названием товара от названия
            # хоть что-то осталось, считаем это названием модели
            if len(_product_name) > 0:
                _main_features.append(_product_name)

            # Выводим общую информацию о товаре
            print('{}: {} ({})'.format(_brand, _product_name, element['name']))
            print('  Осн.признаки: {}'.format(_main_features))
            print('  Доп.признаки: {}'.format(_additional_features))
            print()
            '''

            # Помещаем обработанные элементы в массив для дальнейшей обработки
            # product = ProductToClassify(element['_id'], _brand, _product_name, element['name'])
            # product.copy_additional_features(_additional_features)
            # all_products.append(product)

        # Осуществляем попытку классификации данных
        # classify_products(all_products)

        # Перед тем, как начать классификацию, готовим список наиболее популярных
        # тэгов типа TOKEN_AFTER_COMMA. Часто именно в этих токенах храниться
        # надёжный классификационный признак
        popular_after_commas = _collect_popular_after_commas(_products)

        # Выполняем группировку товаров по "тривиальному индексу", который,
        # в общем случае, содержит тип товара и модель
        groups = {}
        for _token in _products:

            _index = _token.get_trivial_index(popular_after_commas)
            if groups.get(_index) is None:
                groups[_index] = []

            groups[_index].append(_token.original_name)

        print()
        print('---------------- ГРУППИРОВКА ----')
        for _key in list(groups.keys()):

            if len(groups[_key]) > 1:
                print('{}:'.format(_key))
                for _element in groups[_key]:
                    print('  {}'.format(_element))

        print()
        print('---------------- НЕ СГРУППИРОВЛИСЬ ----')
        for _key in list(groups.keys()):

            if len(groups[_key]) == 1:
                print('{}: {}'.format(_key, groups[_key]))

        # Сохраняем выделенный тривиальный индекс в поле 'trivial_index'
        # в базе данных MongoDB
        for _token in _products:

            _index = _token.get_trivial_index(popular_after_commas)
            if _index is None:
                continue

            try:
                db_connection.scrape.playsets.update_one(
                    {'_id': _token.mongo_id},
                    {"$set": {"trivial_index": _index}}, upsert=False
                )

            except OperationFailure as dberror:
                print("Не удалось обновить запись: {}".format(dberror))
        pass


# Функция осуществляет сбор токенов с типом TOKEN_AFTER_COMMA
# и сортирует их по популярности
def _collect_popular_after_commas(_products):

    _pre_after_commas = {}

    # Накапливаем статистику
    for _element in _products:

        for index, _token in enumerate(_element.tokens):

            _value = _token[0]
            _token_type = _token[1]

            if _token_type == ProductTokenizer.TOKEN_AFTER_COMMA:

                if _pre_after_commas.get(_value) is None:
                    _pre_after_commas[_value] = 1
                else:
                    _pre_after_commas[_value] += 1

    # Сортируем элементы
    ordered_entries = OrderedDict(sorted(_pre_after_commas.items(), key=itemgetter(1), reverse=True))
    return list(ordered_entries.keys())


# Функция осуществляет сбор идентификационных данных, основываясь на
# ранее распознанных токенах
def _collect_identification_lists(_products):

    _product_types = {}
    _models = {}
    _trademarks = {}
    _variations = {}

    for _element in _products:

        # Если у товара был выделен тип товара, то сохраняем его в
        # словаре - счётчике популярности использования
        if _element.product_type is not None:

            if _product_types.get(_element.product_type) is None:
                _product_types[_element.product_type] = 1
            else:
                _product_types[_element.product_type] += 1

        _potential_type = None
        for index, _token in enumerate(_element.tokens):

            _value = _token[0]
            _token_type = _token[1]

            # Учитываем потенциальные типы товаров, выделенные при анализе
            # положения бренда в названии товара
            if index == 0:
                _potential_type = _value

            if index == 1 and _token_type == ProductTokenizer.TOKEN_BRAND:

                if _product_types.get(_potential_type) is None:
                    _product_types[_potential_type] = 1
                else:
                    _product_types[_potential_type] += 1

            # Название серий товаров трактуем, как товарные марки
            if _token_type == ProductTokenizer.TOKEN_SET_NAME:

                if _trademarks.get(_value) is None:
                    _trademarks[_value] = 1
                else:
                    _trademarks[_value] += 1

            # Учитываем потенциальные названия моделей
            if _token_type == ProductTokenizer.TOKEN_MODEL:

                if _models.get(_value) is None:
                    _models[_value] = 1
                else:
                    _models[_value] += 1

            # Учитываем потенциальные названия моделей
            if _token_type == ProductTokenizer.TOKEN_AFTER_COMMA:

                if _variations.get(_value) is None:
                    _variations[_value] = 1
                else:
                    _variations[_value] += 1

    # Фильтруем собранные справочники
    pre_models_list = remove_elements_by_threshold(_models)

    # Объединяем часто используемые слова после запятой, которые могут
    # быть либо вариациями, либо торговыми знаками с названиями моделей
    variations_list = remove_elements_by_threshold(_variations)
    trademarks_list = remove_elements_by_threshold(_trademarks)
    trademarks_list.extend(variations_list)

    # Считаем, что качество справочника типов товаров достаточно высокое
    # и к нему можно применять простейшую фильтрацию - удалять варианты,
    # которые встречаются всего по одному разу

    # Если тип товара встречается в списке выделенных товарных знаков, то
    # игнорируем такое значение
    product_types_list = []
    for _type in list(_product_types.keys()):
        if _product_types[_type] > 1:
            if _type not in trademarks_list:
                product_types_list.append(_type)

    product_types_list.sort(key=len, reverse=True)

    # У нас получилось два достаточно качественных справочника - типов товаров
    # и торговых марок. Объединяем оба из этих справочников с тем, чтобы
    # удалить их из названия моделей
    prohibited_words = product_types_list + trademarks_list
    prohibited_words.sort(key=len, reverse=True)

    # Если модель встречается в списке выделенных товарных знаков, то
    # не рассматриваем такое значение, как модель
    models_list = []
    for _model in pre_models_list:

        if _model in prohibited_words:
            continue

        for _p in prohibited_words:

            pos = _model.find(_p)
            if pos >= 0:
                _model = _model[pos + len(_p):].strip()
                break

        if _model is not None and len(_model) > 0:
            models_list.append(_model)

    return product_types_list, models_list, trademarks_list


# Функция осуществляет сравнение всех товаров со всеми и группирует их
def classify_products(products):

    print('Найдены пары:')
    for _first in products:
        for _second in products:

            if _first.id == _second.id:
                continue

            if _first.is_equal(_second):
                print('  {} == {}'.format(_first.original_text, _second.original_text))


if __name__ == "__main__":

    # Подключаемся к базе данных, хранящей результаты обработки XML-данных
    try:

        # Подключаемся к базе данных MongoDB
        uri = 'mongodb://authorizedScrape:authorizedUser$456@77.244.213.247:27017/scrape'
        connection = MongoClient(uri)

        # Осуществляем поиск моделей по строке с полным именем товара

        # ToDo: унифицировать обработку товаров разных типов
        # print_product_types_by_names(connection, ["Pampers"], 1)

        print_product_types_by_names(connection, ["Mattel", "Mattel Games"], 2)

    except ConnectionFailure as dberror:
        print("Не удалось подключиться к базе данных: {}".format(dberror))
