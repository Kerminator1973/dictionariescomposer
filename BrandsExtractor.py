#!/usr/bin/python
# -*- coding: utf-8

import json
import logging
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool    # API поддержки много-поточности в Python-приложении
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


# Вспомогательный класс Encoder, позволяющий экспортировать объекты в JSON
class BrandsEncoder(json.JSONEncoder):
    def default(self, obj):
        return json.JSONEncoder.default(self, obj)


# Функция асинхронной загузки html-страниц. Может быть использована для
# работы в Threading Pool
def async_download_web_page(url):

    try:

        session = requests.Session()
        return session.get(url).text

    except requests.exceptions.RequestException as e:
        logging.error('Сбой в сетевой подсистеме:', e)

    # Если загрузить страницу не удалось, то возвращаем пустое значение
    return None


# Класс-контейнер информации о товарном бренде
class ProductData:
    def __init__(self, _name, _product_url, _count=-1):
        """
        :param str _name: текстовое название бренда
        :param str _product_url: URL-страницы с товарами конкретного бренда
        :param int _count: корличество товаров с указанным брендом. -1 = нет данных
        """
        self.name = _name
        self.product_url = _product_url
        self.count = _count


# Класс осуществляет извлечение списка брендов из online-магазинов
# с целью построения справочников для разбора названий товаров на
# составляющие части по схеме:
# - Product type (P)
# - Brand (B)
# - Model (M)
class BrandsExtractor:

    def __init__(self):
        pass

    @staticmethod
    def scrape_detmir_brands(mongodb):

        # Накопленные бренды будут сохранены в отдельный контейнер
        all_brands = []

        try:

            # Храним общую ссылку на страницу брендов из-за протокола.
            # Часть страницы доступна через http://, но как только начинается
            # работа с конкретными товарами, сайт переходит на использование
            # протокола https:// для того, чтобы обеспечить приватность
            # действий покупателя
            detmir_brands_href = 'http://www.detmir.ru/brands/'

            # Загружаем страницу с брендами, представленными в магазине
            session = requests.Session()
            response = session.get(detmir_brands_href)
            if response.status_code != requests.codes.ok:
                logging.error('Status-code отличается от [200]: {}'.format(response.status_code))
                return False

            soup = BeautifulSoup(response.text, 'html.parser')

            brands_list = soup.find('div', attrs={'class': 'brand_list_all'})
            if brands_list is None:
                logging.error('Отсутствует маркер описания брендов')
                return False

            print('Данные о брендах загружены. Начинается их обработка')

            # Проходим по списку брендов и выделяем название бренда и URL,
            # который ведёт на страницу товаров бренда
            for element in brands_list.findAll('ul', attrs={'class': 'simbol_item'}):

                brand_href = element.find('a', attrs={'href': True})
                if brand_href is not None:

                    brand_text = brand_href.getText().strip()
                    all_brands.append(ProductData(brand_text, brand_href['href']))

            # Проверяем общее количество извлечённых брендов и сверяем его
            # с количеством, задекларированным магазином
            brands_count = 0

            head_div = soup.find('div', attrs={'class': 'head'})
            if head_div is not None:

                all_brands_div = head_div.find('li', attrs={'class': 'btn_brand_all'})
                if all_brands_div is not None:

                    text_to_parse = all_brands_div.getText().strip()

                    left_bracket = text_to_parse.find('(')
                    right_bracket = text_to_parse.find(')')
                    if right_bracket > 0 and right_bracket > left_bracket > 0:

                        try:
                            brands_count = int(text_to_parse[left_bracket + 1: right_bracket])
                        except ValueError:
                            pass

            if brands_count == 0:
                logging.error('Не удалось извлечь информацию о количестве брендов в магазине')
                return False

            if brands_count != len(all_brands):
                logging.error('Не совпадает количество фактически извлечённых брендов с оценкой магазина')
                return False

            print('Извлечено {} брендов'.format(brands_count))

            # Сохраняем накопленные данные в коллекцию MongoDB
            mongodb.detmir_brands.drop()
            for brand in all_brands:

                mongodb.detmir_brands.insert_one(
                    {
                        "name": brand.name,
                        "url": brand.product_url
                    })

            return True

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме:', e)

        return False

    @staticmethod
    def scrape_ds_brands(mongodb):

        all_brands = []

        try:

            ds_brands_href = 'http://www.dochkisinochki.ru/brands/'

            session = requests.Session()
            response = session.get(ds_brands_href)
            if response.status_code != requests.codes.ok:
                logging.error('Status-code отличается от [200]: {}'.format(response.status_code))
                return False

            soup = BeautifulSoup(response.text, 'html.parser')

            for element in soup.findAll('div', attrs={'class': 'cataloglist_group_item_link'}):

                brand_href = element.find('a', attrs={'href': True})
                if brand_href is not None:

                    brand_text = brand_href.getText().strip()
                    all_brands.append(ProductData(brand_text, brand_href['href']))

            if len(all_brands) == 0:
                logging.error('Не удалось извлечь информацию о брендах')
                return False

            print('Извлечено брендов: {}'.format(len(all_brands)))

            # Сохраняем накопленные данные в коллекцию MongoDB
            mongodb.dochkisinochki_brands.drop()
            for brand in all_brands:

                mongodb.dochkisinochki_brands.insert_one(
                    {
                        "name": brand.name,
                        "url": brand.product_url
                    })

            return True

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме:', e)

        return False

    @staticmethod
    def scrape_akusherstvo_brands(mongodb):

        all_brands = []

        try:

            ds_brands_href = 'http://www.akusherstvo.ru/brands/'

            session = requests.Session()
            response = session.get(ds_brands_href)
            if response.status_code != requests.codes.ok:
                logging.error('Status-code отличается от [200]: {}'.format(response.status_code))
                return False

            soup = BeautifulSoup(response.text, 'html.parser')

            for brands_list in soup.findAll('div', attrs={'class': 'brands-toc-items-col'}):

                for element in brands_list.findAll('p', attrs={'class': '__title'}):

                    brand_href = element.find('a', attrs={'href': True})
                    if brand_href is not None:

                        # Для каждого элемента выделяем название бренда, количество товаров
                        # у бренда и URL на страницу бренда
                        brand_text = brand_href.getText().strip()

                        left_bracket = brand_text.find('(')
                        right_bracket = brand_text.find(')')

                        products_count = -1
                        if right_bracket > 0 and right_bracket > left_bracket > 0:

                            _name = brand_text[:left_bracket].strip()

                            try:
                                products_count = int(brand_text[left_bracket + 1: right_bracket])
                            except ValueError:
                                pass

                            brand_text = _name

                        all_brands.append(ProductData(brand_text, brand_href['href'], products_count))

            if len(all_brands) == 0:
                logging.error('Не удалось извлечь информацию о брендах')
                return False

            print('Извлечено брендов: {}'.format(len(all_brands)))

            # Сохраняем накопленные данные в коллекцию MongoDB
            mongodb.akusherstvo_brands.drop()
            for brand in all_brands:

                mongodb.akusherstvo_brands.insert_one(
                    {
                        "name": brand.name,
                        "url": brand.product_url,
                        "count": brand.count
                    })

            return True

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме:', e)

        return False

    @staticmethod
    def scrape_korablik_brands(mongodb):

        all_brands = []

        try:

            # Бренды в Кораблике сразу находятся в зоне https
            ds_brands_href = 'https://www.korablik.ru/brands'

            session = requests.Session()
            response = session.get(ds_brands_href)
            if response.status_code != requests.codes.ok:
                logging.error('Status-code отличается от [200]: {}'.format(response.status_code))
                return False

            # Извлекаем первую страницу брендов магазина
            if not BrandsExtractor.parse_korablik_brands_page(response.text, all_brands):
                return False

            # Обрабатываем paginator с целью определения общего количества страниц брендов
            soup = BeautifulSoup(response.text, 'html.parser')

            paginator = soup.find('div', attrs={'class': 'paginator'})
            if paginator is None:
                logging.error('Отсутствует paginator, что является ошибкой')
                return False

            # Ссылки на другие страницы брендов выглядят так: <a href="/brands?page=2">2</a>
            max_page_no = 0
            for page in paginator.findAll('a', attrs={'href': True}):

                page_no = 0
                try:
                    page_no = int(page.getText().strip())
                except ValueError:
                    pass

                if page_no > max_page_no:
                    max_page_no = page_no

            print('Всего страниц с брендами: {}'.format(max_page_no))

            # После того, как стало известно общее количество страниц с брендами,
            # имеет смысл отработать их все, за исключением самой первой

            # Используем List Comprehension для формирования полного списка загружаемых
            # страниц с брендами магазина
            all_brand_pages = ['http://www.korablik.ru/brands?page={}'.format(page_no)
                               for page_no in range(2, max_page_no)]

            # Формируем pool рабочий потоков, задачей которых будет являться
            # загрузка html-страниц с данными товара
            pool = ThreadPool(2)

            # Результаты работы будут накапливаться в отдельном списке
            all_html_documents = pool.map(async_download_web_page, all_brand_pages)

            # Устанавливаем флаг завершения работы и ждём, когда
            # все рабочие потоки завершат свою работу
            pool.close()
            pool.join()

            # Обрабатываем загруженные HTML-документы с описанием брендов
            for html_document in all_html_documents:
                if html_document is None:
                    logging.error('Ошибка загрузки html-страницы с описанием брендов')
                    return False

                if not BrandsExtractor.parse_korablik_brands_page(html_document, all_brands):
                    logging.error('Ошибка разбора html-страницы с описанием брендов')
                    return False

            if len(all_brands) == 0:
                logging.error('Не удалось извлечь информацию о брендах')
                return False

            print('Извлечено брендов: {}'.format(len(all_brands)))

            # Сохраняем накопленные данные в коллекцию MongoDB
            mongodb.korablik_brands.drop()
            for brand in all_brands:

                mongodb.korablik_brands.insert_one(
                    {
                        "name": brand.name,
                        "url": brand.product_url
                    })

            return True

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме:', e)

        return False

    @staticmethod
    def parse_korablik_brands_page(html_page, all_brands):
        """
        :param str html_page: загруженная HTML-страница
        :param list all_brands: контейнер с выделенными брендами
        :return: boolean
        """
        soup = BeautifulSoup(html_page, 'html.parser')

        brands_list = soup.find('div', attrs={'class': 'body__catalog_brand_page'})
        if brands_list is None:
            logging.error('Отсутствует маркер описания брендов')
            return False

        # Проходим по списку брендов и выделяем название бренда и URL,
        # который ведёт на страницу товаров бренда
        for element in brands_list.findAll('div', attrs={'class': 'body__catalog-item'}):

            if element.get('rev') is not None:

                brand_href = element['rev']
                span_name = element.find('span', attrs={'class': 'title_catalog_items'})

                if span_name is not None:
                    brand_text = span_name.getText().strip()
                    all_brands.append(ProductData(brand_text, brand_href))

        return True

    # Функция добавляет накопленные бренды в коллекцию
    @staticmethod
    def insert_brands_to_collection(mongo_collection, brands_list, collection_name):

        collection = mongo_collection.find({})

        if collection.count() == 0:

            print('В коллекции "{}" отсутствуют данные'.format(collection_name))
            return

        for element in collection:

            _name = element['name'].upper().strip()
            if _name not in brands_list:
                brands_list.append(_name)

    # Функция выводит все ранее накопленные бренды на экран
    @staticmethod
    def show_imported_brands(mongodb):

        all_brands = []

        BrandsExtractor.insert_brands_to_collection(mongodb.detmir_brands, all_brands, "Детский мир")
        BrandsExtractor.insert_brands_to_collection(mongodb.dochkisinochki_brands, all_brands, "Дочки-сыночки")
        BrandsExtractor.insert_brands_to_collection(mongodb.akusherstvo_brands, all_brands, "Акушерство")
        BrandsExtractor.insert_brands_to_collection(mongodb.korablik_brands, all_brands, "Кораблик")

        all_brands.sort(reverse=False)

        # Сохраняем накопленные данные в виде JSON-объекта
        with open('dirty_brands.json', 'w', encoding='utf8') as outfile:
            json.dump(all_brands, outfile, cls=BrandsEncoder, ensure_ascii=False)


if __name__ == "__main__":

    # Создаём connector для MongoDB. По умолчанию, подключаемся к копии MongoDB
    # установленной на localhost с портом подключения 27017. Если потребуется
    # подключиться к другой базе данных, нужно будет параметризовать вызов
    # MongoClient, например, так:
    #       client = MongoClient("mongodb://mongodb0.example.net:27017")
    try:
        client = MongoClient()

        # Подключаемся непосредственно к базе данных
        db = client.online_shopping_scrapers

        # Создаём объект, осуществляющий извлечение названий брендов и url с сайтов online-магазинов
        scrapers = BrandsExtractor()

        print('Осуществляется извлечение информации о брендах:')

        print('Детский Мир...')
        logging.warning('Детский Мир...')
        result = scrapers.scrape_detmir_brands(db)
        if not result:
            print('Возникла ошибка при извлечении информации о брендах. См. лог-файл')
            exit(1)

        print('Дочки-сыночки...')
        logging.warning('Дочки-сыночки...')
        result = scrapers.scrape_ds_brands(db)
        if not result:
            print('Возникла ошибка при извлечении информации о брендах. См. лог-файл')
            exit(2)

        print('Акушерство...')
        logging.warning('Акушерство...')
        result = scrapers.scrape_akusherstvo_brands(db)
        if not result:
            print('Возникла ошибка при извлечении информации о брендах. См. лог-файл')
            exit(3)

        print('Кораблик...')
        logging.warning('Кораблик...')
        result = scrapers.scrape_korablik_brands(db)
        if not result:
            print('Возникла ошибка при извлечении информации о брендах. См. лог-файл')
            exit(4)

        print('Успешное извлечение информации о брендах')

        # Вызов функции позволяет сохранить накопленные бренды в файл 'dirty_brands.json'
        BrandsExtractor.show_imported_brands(db)

    except ConnectionFailure as dberror:
        print("Не удалось подключиться к базе данных: {}".format(dberror))

    # Следующий шаг - выделение популярных брендов - они будут использоваться для выявления
    # вариаций товаров и создания списка синонимов типов товаров

    # Также необходимо осуществить поиск синонимов, например: ROEMER и RÖMER
