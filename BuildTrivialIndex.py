#!/usr/bin/python
# -*- coding: utf-8

import re
import time
import argparse
import urllib.parse
from collections import OrderedDict
from operator import itemgetter
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, OperationFailure
from transliterate import translit  # Установка библиотеки: pip3 install transliterate
import string                       # Package включает различные таблицы символов
import collections                  # Package содержащий дополнительные коллекции, например, defaultdict
import unicodedata                  # Package для нормализации Unicode-строк (например, удаления accents)
from ProductTokenizer import ProductTokenizer
from CleaningHelper import remove_elements_by_threshold
from CheckSourceIntegrity import find_brand_synonyms


def build_trivial_index(db_connection, brands_list):
    """
    :param db_connection: connector к базе данных
    :param list brands_list: список брендов (используется для создания условия отбора из базы данных)
    """

    start_time = time.time()

    # Определяем список глобальных торговых марок, т.е. таких, которые лицензируются
    # разными производителями товаров
    global_trademarks = ['ЗВЁЗДНЫЕ ВОЙНЫ', 'THOMAS&FRIENDS', 'ТОМАС И ЕГО ДРУЗЬЯ', 'ТАЧКИ 2',
                         'DC COMICS', 'ЧЕРЕПАШКИ НИНДЗЯ']

    # Динамически составляем список условий, в зависимости от количества указанных брендов
    if len(brands_list) == 1:

        condition = {"brand": re.compile(brands_list[0], re.IGNORECASE)}

    else:

        # В MongoDB команда проверки нескольких условий выглядит следующим образом:
        #   { $or: [ { <expression1> }, { <expression2> }, ... , { <expressionN> } ] }
        # Например:
        #   db.inventory.find( { $or: [ { quantity: { $lt: 20 } }, { price: 10 } ] } )
        #
        # Ссылка: https://docs.mongodb.com/manual/reference/operator/query/or/

        elements = []
        for _ in brands_list:
            elements.append({"brand": re.compile(_, re.IGNORECASE)})

        condition = {"$or": elements}

    # Отбираем элементы с разным написанием бренда из базы данных
    playsets = db_connection.scrape.playsets.find(condition)
    products_count = playsets.count()
    print('Товаров с указанным брендом: {}'.format(products_count))

    # Осуществляем попытку выделить глобальные торговые марки
    global_trademarks.extend(find_english_trademarks(playsets, brands_list))

    # Разбираем названия товаров на отдельные токены
    _products = _tokenize_products(playsets.rewind(), brands_list, global_trademarks)

    # Выделяем список наиболее популярных товарных знаков и вариаций
    _popular_after_commas = _collect_popular_after_commas(_products)

    # Группируем товары
    _group_products(_products, _popular_after_commas)

    # Обновляем информацию в базе данных
    _update_products(db_connection, _products, _popular_after_commas)

    # ИССЛЕДОВАТЕЛЬСКАЯ ОПЕРАЦИЯ:
    # Выводим профиль популярности названий в "хлебных крошках"
    # _display_breadcrumb_profile(playsets.rewind())

    print("Процесс формирования индекса занял: {} мс.".format(
        int(round((time.time() - start_time) * 1000))))


# Функция осуществляет поиск торговых знаков, как последовательности слов,
# написанных латинскими символами
def find_english_trademarks(playsets, brands_list):
    """
    :param playsets: connector к базе данных
    :param list brands_list: список брендов
    """

    upper_brands = [_.upper for _ in brands_list]
    acceptable_brand_chars = string.ascii_letters + '-&\''

    _candidates = {}
    for element in playsets:

        if element.get('name') is None:
            continue

        _words = []

        _name = element['name'].upper().replace(',', ' ').replace('  ', ' ')
        _name = _name.replace(' &', '&').replace('& ', '&').replace('’', "'")
        for _w in _name.split(' '):

            has_only_letters = True
            for _c in _w:
                if _c not in acceptable_brand_chars:
                    if _c != '\'':
                        has_only_letters = False
                        break

            if not has_only_letters:

                # Если найдено слово, в котором есть символы кириллицы,
                # или цифры, то если уже были найдены какие-то слова
                # содержащие только символы латинского алфавита, то
                # останавливаем поиск и считаем найденное потенциальным
                # товарным знаком
                if len(_words) > 0:
                    _add_words_to_trademark_candidates(_words, _candidates, upper_brands)
                    _words = []

                # Если слово состоит не только из латинских символов, то
                # пропускаем его
                continue

            # Слово состоящее из латинских символов включаем в состав
            # потенциального товарного знака
            _words.append(_w)

        if len(_words) > 0:
            _add_words_to_trademark_candidates(_words, _candidates, upper_brands)

    # Удаляем шум, формируем список потенциальных торговых знаков
    elements = remove_elements_by_threshold(_candidates, threshold=4, remove_simplest=False)

    # Два раза выполняем функцию разделения торговых марок на две части, чтобы
    # решить проблемы с такими названиями, как "HOT WHEELS STAR WARS"
    # и "STAR WARS HOT WHEELS"
    elements = _split_trademarks(elements)
    elements = _split_trademarks(elements)

    print()
    print('--- СЛОВА ИЗ ЛАТИНСКИХ СИМВОЛОВ, КОТОРЫЕ ВЫГЛЯДЯТ КАК TRADEMARK ---')
    for _ in elements:
        print('  {}'.format(_))

    return elements


# Вспомогательная функция разделяет такие названия как "HOT WHEELS STAR WARS"
# и "STAR WARS HOT WHEELS" на "STAR WARS" и "HOT WHEELS"
def _split_trademarks(elements):

    result = []
    for _e in elements:

        has_to_append = True
        for _g in elements:
            if _e != _g:
                pos = _e.find(_g)
                if pos >= 0:

                    if pos == 0:

                        first_part = _e[:len(_g)]
                        second_part = _e[len(_g):]

                    else:

                        first_part = _e[:pos]
                        second_part = _e[pos:]

                    to_append = first_part.strip()
                    if to_append not in result:
                        result.append(to_append)

                    to_append = second_part.strip()
                    if to_append not in result:
                        result.append(to_append)

                    has_to_append = False

        if has_to_append:
            to_append = _e.strip()
            if to_append not in result:
                result.append(to_append)

    return result


# Вспомогательная функция, объединяет слова в блок и добавляет в список кандидатов
# на товарные знаки
def _add_words_to_trademark_candidates(_words, _candidates, upper_brands):

    _trademark_candidate = ' '.join(_words)

    if _trademark_candidate == '-':
        return

    # Бренд игнорируем
    if _trademark_candidate not in upper_brands:

        if _candidates.get(_trademark_candidate) is None:
            _candidates[_trademark_candidate] = 1
        else:
            _candidates[_trademark_candidate] += 1


# Функция осуществляет сбор идентификационных данных, основываясь на
# ранее распознанных токенах
def _collect_identification_lists(_products):
    """
    :param list _products: коллекция названий товаров, разобранных на отдельные токены
    """

    _product_types = {}
    _models = {}
    _trademarks_as_serial = {}
    _trademarks_by_type = {}
    _variations = {}

    for _element in _products:

        # Если у товара был выделен тип товара, то сохраняем его в
        # словаре - счётчике популярности использования
        if _element.product_type is not None:

            if _product_types.get(_element.product_type) is None:
                _product_types[_element.product_type] = 1
            else:
                _product_types[_element.product_type] += 1

        if _element.trademark_candidate is not None:

            if _trademarks_by_type.get(_element.trademark_candidate) is None:
                _trademarks_by_type[_element.trademark_candidate] = 1
            else:
                _trademarks_by_type[_element.trademark_candidate] += 1

        _potential_type = None
        for index, _token in enumerate(_element.tokens):

            _value = _token[0]
            _token_type = _token[1]

            # Учитываем потенциальные типы товаров, выделенные при анализе
            # положения бренда в названии товара
            if index == 0:
                _potential_type = _value

            if index == 1 and _token_type == ProductTokenizer.TOKEN_BRAND:

                if _product_types.get(_potential_type) is None:
                    _product_types[_potential_type] = 1
                else:
                    _product_types[_potential_type] += 1

            # Название серий товаров трактуем, как товарные марки
            if _token_type == ProductTokenizer.TOKEN_SET_NAME:

                if _trademarks_as_serial.get(_value) is None:
                    _trademarks_as_serial[_value] = 1
                else:
                    _trademarks_as_serial[_value] += 1

            # Учитываем потенциальные названия моделей
            if _token_type == ProductTokenizer.TOKEN_MODEL:

                if _models.get(_value) is None:
                    _models[_value] = 1
                else:
                    _models[_value] += 1

            # Учитываем потенциальные названия моделей
            if _token_type == ProductTokenizer.TOKEN_AFTER_COMMA:

                if _variations.get(_value) is None:
                    _variations[_value] = 1
                else:
                    _variations[_value] += 1

    # Формируем списки моделей-синонимов и синонимов товарных знаков
    print()
    print('--- СИНОНИМЫ МОДЕЛЕЙ ---')
    model_synonyms = _get_synonyms(_models)

    print()
    print('--- ВАРИАЦИИ/ТОВАРНЫЕ ЗНАКИ ---')
    variation_synonyms = _get_synonyms(_variations)

    # Фильтруем собранные справочники
    pre_models_list = remove_elements_by_threshold(_models)

    # Объединяем часто используемые слова после запятой, которые могут
    # быть либо вариациями, либо торговыми знаками с названиями моделей

    trademarks_list = remove_elements_by_threshold(_trademarks_as_serial)
    trademarks_list.extend(remove_elements_by_threshold(_trademarks_by_type, 5))
    trademarks_list.extend(remove_elements_by_threshold(_variations))

    # Считаем, что качество справочника типов товаров достаточно высокое
    # и к нему можно применять простейшую фильтрацию - удалять варианты,
    # которые встречаются всего по одному разу

    # Если тип товара встречается в списке выделенных товарных знаков, то
    # игнорируем такое значение

    _pre_product_types = remove_elements_by_threshold(_product_types)

    product_types_list = []
    for _type in _pre_product_types:
        if _type not in trademarks_list:
            product_types_list.append(_type)

    product_types_list.sort(key=len, reverse=True)

    # У нас получилось два достаточно качественных справочника - типов товаров
    # и торговых марок. Объединяем оба из этих справочников с тем, чтобы
    # удалить их из названия моделей
    prohibited_words = product_types_list + trademarks_list
    prohibited_words.sort(key=len, reverse=True)

    # Если модель встречается в списке выделенных товарных знаков, то
    # не рассматриваем такое значение, как модель
    models_list = []
    for _model in pre_models_list:

        if _model in prohibited_words:
            continue

        for _p in prohibited_words:

            pos = _model.find(_p)
            if pos >= 0:
                _model = _model[pos + len(_p):].strip()
                break

        if _model is not None and len(_model) > 0:
            models_list.append(_model)

    return product_types_list, models_list, trademarks_list, model_synonyms, variation_synonyms


# Функция выделяет список типов товаров и серий, основываясь на наиболее популярных
# частях названия продуктов
def _collect_series_exp(_products):
    """
    :param list _products: коллекция названий товаров, разобранных на отдельные токены
    """

    _ac_collection = {}
    for _element in _products:

        # Нас интересуют только две комбинации токенов:
        # 1) After Comma + Model, например:
        #   "ИГРУШЕЧНЫЙ ТРЕК ГОЛОВОКРУЖИТЕЛЬНЫЙ ПРЫЖОК"
        #   "ИГРУШЕЧНЫЙ ТРЕК DRIFT KING"
        # 2) Product Type + Model, например:
        #   "ВСПЫШ И ЕГО ДРУЗЬЯ МУСОР"
        _after_comma = []
        _model = None

        ignore = False
        for _token in _element.tokens:

            _value = _token[0]
            _tt = _token[1]

            if _tt == ProductTokenizer.TOKEN_BRAND or _tt == ProductTokenizer.TOKEN_PRODUCT_TYPE:
                continue

            if _tt == ProductTokenizer.TOKEN_AFTER_COMMA:
                _after_comma.append(_value)

            elif _tt == ProductTokenizer.TOKEN_MODEL:
                _model = _value

            else:

                # Если есть токены других типов, такие товары нас не интересуют
                ignore = True

        if ignore:
            continue

        # Игнорируем варианты, в которых встречаются тип товара и after comma
        if len(_after_comma) == 0 or _model is None:
            continue

        # Насыщаем справочники информацией
        for _ac in _after_comma:

            if _ac_collection.get(_ac) is None:
                _ac_collection[_ac] = []

            if _model not in _ac_collection[_ac]:
                _ac_collection[_ac].append(_model)

    pass


# Функция возвращает словарь синонимов, основываясь на словаре популярности
def _get_synonyms(_popularity_dict):
    """
    :param dict _popularity_dict: ключ - некоторое название, значение - кол-во использований
    :return dict: ключ - некоторое название, значение - самое популярное название
    """

    # Для каждого элемента словаря будут построены все разумные
    # варианты написания
    modifications = {}

    # Таблица символов для преобразования diacritics в латинские символы
    diacritics = collections.defaultdict(lambda: None)
    diacritics.update({
        ord(u'Ä'): u'AE',
        ord(u'Ö'): u'OE',
        ord(u'Ü'): u'UE',
        ord(u'ß'): u'SS',
    })
    diacritics.update(dict(zip(map(ord, string.ascii_uppercase), string.ascii_uppercase)))
    diacritics.update(dict(zip(map(ord, string.digits), string.digits)))

    # Генерируем все возможные альтернативные написания названия
    for _s in list(_popularity_dict.keys()):

        variants = []

        # Символы пунктуации заменяем на пробелы
        value = _s.replace('-', ' ')
        if value != _s:
            variants.append(value)

        # Удаляем символы-разделители
        value = _s.replace('-', '')
        if value != _s:
            variants.append(value)

        # Убираем символы "copyright", "registered", "trademark".
        # См.: http://www.fileformat.info/info/unicode/char/00ae/index.htm
        value = _s.replace(u"\u2122", '')
        value = value.replace(u"\u00A9", '')
        value = value.replace(u"\u00AE", '')
        value = value.replace('!', '')
        if value != _s:
            variants.append(value)

        # Генерируем вариант с удалением accents, т.е. указанием ударения
        value = remove_accents(_s)
        if value != _s:
            variants.append(value)

        # Генерируем варианты с подменой диакритических символов и транслитерацией
        modified_brand = _s.translate(diacritics, )

        if modified_brand == _s:

            value = translit(_s, "ru")
            if value != _s:
                variants.append(value)

        else:

            variants.append(modified_brand)

            value = translit(modified_brand, "ru")
            if value != modified_brand:
                variants.append(value)

        modifications[_s] = variants

    # Осуществляем сравнение всех названий со всеми, с целью нахождения
    # пересечений в производных (сгенерированных) названиях
    _clusters = []

    for _s in list(_popularity_dict.keys()):
        for key in list(modifications.keys()):

            if key == _s:
                continue

            for variant in modifications[key]:

                if variant == _s:

                    # Найдена пара названий-синонимов. Теперь их нужно поместить
                    # в кластер синонимов. Пар с совпадениями названий может быть
                    # достаточно много и имеет смысл группировать все подобные
                    # пересечения в одном массиве
                    is_found = False

                    for cluster in _clusters:

                        if _s in cluster or key in cluster:

                            is_found = True

                            if _s not in cluster:
                                cluster.append(_s)

                            if key not in cluster:
                                cluster.append(key)

                            break

                    if not is_found:

                        new_elements = [_s, key]
                        _clusters.append(new_elements)

    # После того, как все кластеры эквивалентных названий сформированы,
    # генерируем словарь-перекодировки
    substitution = {}

    for elements in _clusters:

        # Для каждого названия находим самую популярную альтернативу
        _max_items_count = 0
        _the_most_popular = ''
        for _s in elements:
            if _popularity_dict[_s] > _max_items_count:
                _max_items_count = _popularity_dict[_s]
                _the_most_popular = _s

        # Выводим наиболее популярное название и все варианты, с указанием их количества
        str_equivalents_with_count = ['{} ({})'.format(x, _popularity_dict[x]) for x in elements]
        print('  "{}": {}'.format(_the_most_popular, str_equivalents_with_count))

        for _s in elements:
            if _s != _the_most_popular:
                substitution[_s] = _the_most_popular

    # Отладочная трассирока
    # for _s in list(substitution.keys()):
    #    print('  "{}" => "{}"'.format(_s,substitution[_s]))

    return substitution


#
def remove_accents(data):
    return ''.join(x for x in unicodedata.normalize('NFKD', data) if x in string.ascii_letters)


# Функция осуществляет разделение названий товаров, считанных из базы данных
# на список токенов, которые могут быть использованы для группировки товаров
def _tokenize_products(playsets, brands_list, global_trademarks):
    """
    :param playsets: connector к базе данных
    :param list brands_list: список брендов. Используются при разборе названия товара
    :param list global_trademarks: список популярных товарных знаков
    """

    _products = []
    for element in playsets:

        if element.get('name') is None:
            continue

        # Осуществляем разбор строки текста на отдельные токены
        tokenizer = ProductTokenizer(brands_list)
        tokenizer.tokenize_trivial(element)
        _products.append(tokenizer)

    # После обработки данных токенайзером, пытаемся выделить вспомогательные словари.
    product_types_list, models_list, trademarks_list, \
        model_synonyms, variation_synonyms = _collect_identification_lists(_products)

    for _t in global_trademarks:
        if _t not in trademarks_list:
            trademarks_list.append(_t)

    print()
    print('Выделенные типы товаров:')
    for _item in product_types_list:
        print('  {}'.format(_item))

    print()
    print('Выделенные названия моделей:')
    for _item in models_list:
        print('  {}'.format(_item))

    print()
    print('Выделенные торговые марки и вариации:')
    for _item in trademarks_list:
        print('  {}'.format(_item))

    print('----------------')
    synonyms_count = 0
    for _p in _products:
        # Вторая стадия - выделяем элементы, используя справочники
        _p.correct_by_dictionaries(product_types_list, models_list, trademarks_list)

        # Третья стадия - уточняем типы, используя эвристические схемы,
        # а также заменяем значения токенов-синонимов
        synonyms_count += _p.finalize(model_synonyms, variation_synonyms)
        print(_p)

    print()
    print('--- ЗАМЕНЕНО СИНОНИМОВ: {} ---'.format(synonyms_count))

    # Ещё один экспериментальный код - выделение названий серий и типов товаров
    # по частично разобранным на токены названиям
    # print()
    # print('--- ЭКСПЕРИМЕНТАЛЬНЫЙ ПОИСК ТИПОВ ТОВАРОВ И ИХ СЕРИЙ: ---')
    # _collect_series_exp(_products)

    return _products


# Функция осуществляет группировку товаров, используя полученные токены
def _group_products(_products, _popular_after_commas):
    """
    :param list _products: коллекция названий товаров, разобранных на отдельные токены
    :param list _popular_after_commas: список наиболее популяных товарных знаков и вариаций
    """

    # Перед тем, как начать классификацию, готовим список наиболее популярных
    # тэгов типа TOKEN_AFTER_COMMA. Часто именно в этих токенах храниться
    # надёжный классификационный признак

    # Выполняем группировку товаров по "тривиальному индексу", который,
    # в общем случае, содержит тип товара и модель
    groups = {}
    for _token in _products:

        _index = _token.get_trivial_index(_popular_after_commas)
        if groups.get(_index) is None:
            groups[_index] = []

        groups[_index].append(_token.original_name)

    print()
    print('---------------- ГРУППИРОВКА ----')
    grouped_count = 0
    for _key in list(groups.keys()):

        if len(groups[_key]) > 1:
            grouped_count += len(groups[_key])
            print('{}:'.format(_key))
            for _element in groups[_key]:
                print('  {}'.format(_element))

    print()
    ungrouped_count = 0
    print('---------------- НЕ СГРУППИРОВЛИСЬ ----')
    for _key in list(groups.keys()):

        if len(groups[_key]) == 1:
            print('{}: {}'.format(_key, groups[_key]))
            ungrouped_count += 1

    print()
    total = grouped_count + ungrouped_count
    print('--- В ГРУППЕ: {}, ВСЕГО: {}'.format(grouped_count, total))
    print('КОЭФ.ГРИППИРОВКИ - %.2f%%' % (grouped_count * 100 / total))


# Функция осуществляет сбор токенов с типом TOKEN_AFTER_COMMA
# и сортирует их по популярности
def _collect_popular_after_commas(_products):

    _pre_after_commas = {}

    # Накапливаем статистику
    for _element in _products:

        for index, _token in enumerate(_element.tokens):

            _value = _token[0]
            _token_type = _token[1]

            if _token_type == ProductTokenizer.TOKEN_AFTER_COMMA:

                if _pre_after_commas.get(_value) is None:
                    _pre_after_commas[_value] = 1
                else:
                    _pre_after_commas[_value] += 1

    # Сортируем элементы
    ordered_entries = OrderedDict(sorted(_pre_after_commas.items(), key=itemgetter(1), reverse=True))
    return list(ordered_entries.keys())


def _update_products(db_connection, _products, _popular_after_commas):
    """
    :param db_connection: connector к базе данных
    :param list _products: коллекция названий товаров, разобранных на отдельные токены
    :param list _popular_after_commas: список наиболее популяных товарных знаков и вариаций
    """

    # Сохраняем выделенный тривиальный индекс в поле 'trivial_index'
    # в базе данных MongoDB
    for _token in _products:

        _index = _token.get_trivial_index(_popular_after_commas)
        if _index is None:
            continue

        try:
            db_connection.scrape.playsets.update_one(
                {'_id': _token.mongo_id},
                {"$set": {"trivial_index": _index}}, upsert=False
            )

        except OperationFailure as err:
            print("Не удалось обновить запись: {}".format(err))


# Функция выводит на экран самые популярные "хлебные крошки", которые
# встречаются у товаров конкретного бренда
def _display_breadcrumb_profile(_products):
    """
    :param _products: коллекция товаров (на движке MongoDB)
    """

    entries = {}

    for element in _products:

        if element.get('breadcrumb') is None:
            return

        _breadcrumb = element['breadcrumb'].strip().split('/')
        if len(_breadcrumb) == 0:
            return

        for _bc in _breadcrumb[-2:]:

            if entries.get(_bc) is None:
                entries[_bc] = 1
            else:
                entries[_bc] += 1

    # Сортируем элементы по популярности
    print()
    print('------------- ПОПУЛЯРНЫЕ BREADCRUMB')

    ordered_entries = OrderedDict(sorted(entries.items(), key=itemgetter(1), reverse=True))
    for _e in list(ordered_entries.keys()):
        print('  {} ({})'.format(_e, ordered_entries[_e]))


if __name__ == "__main__":

    # Параметрами вызова утилиты являются база данных и список брендов,
    # принадлежащих одному производителю
    parser = argparse.ArgumentParser(description='Scraping.ArgumentParser')
    parser.add_argument('host', type=str, default='')
    parser.add_argument('--port', type=int, default=27017)
    parser.add_argument('--admindb', type=str, default='scrape')
    parser.add_argument('--login', type=str, default='')
    parser.add_argument('--pwd', type=str, default='')
    parser.add_argument('--brands', type=str, default=None)

    args = parser.parse_args()

    # Определяем URI базы данных, хранящей справочники товаров online-магазинов
    if args.host is None or len(args.host) == 0:
        print('Не указан IP-адрес сервера базы данных MongoDB')
        exit(1)

    if args.admindb is None or len(args.admindb) == 0:
        print('Не указана база данных администирования прав пользователей (--admindb)')
        exit(1)

    if args.login is None or len(args.login) == 0:
        print('Не указано имя пользователя (--login)')
        exit(1)

    if args.pwd is None or len(args.pwd) == 0:
        print('Не указан пароль (--pwd)')
        exit(1)

    # Существует разница в поведении mongopy в Windows и Linux.
    # Window более щадащая и позволяет не преобразовывать пароль в формат http,
    # в большинстве случаев. Linux - более строгий и это причина, по которой
    # URI формируется в скрипте, а не получается целиком из командной строки
    username = urllib.parse.quote_plus(args.login)
    password = urllib.parse.quote_plus(args.pwd)
    uri = "mongodb://%s:%s@%s:%d/%s" % (username, password, args.host, args.port, args.admindb)

    # Определяем список брендов, которые следует обработать. Символ-разделитель
    # брендов - запятая
    if args.brands is None:

        print('Список брендов не указан, начинаем обработку всех брендов из базы данных')

        try:
            # Подключаемся к базе данных и строим индексы для всех брендов
            connection = MongoClient(uri)
            all_brands, brand_count, brands_clusters = find_brand_synonyms(connection)

            # Сначала формируем для загрузки список брендов у которых есть синонимы.
            # Попутно помещаем все написания бренда в список обработанных. Затем, все
            # бренды, которых нет в списке обработанных добавляем в список загрузки

            brands_processed = []
            for _brands in brands_clusters:

                # Необходимо найти самое популярное написание бренда, чтобы
                # использовать его при формировании Trivial Index

                _max_count = 0
                _most_popular_brand = ''
                for _b in _brands:
                    if brand_count[_b] > _max_count:
                        _max_count = brand_count[_b]
                        _most_popular_brand = _b

                _ordered_list = [_most_popular_brand]
                _ordered_list.extend([_b for _b in _brands if _b != _most_popular_brand])

                # print('  {}'.format(_brands))
                build_trivial_index(connection, _brands)
                brands_processed.extend(_brands)

            for _brand in all_brands:
                if _brand not in brands_processed:

                    # Ограничение в количестве товаров нужно для того,
                    # чтобы статистические алгоритмы работали
                    if brand_count[_brand] > 40:
                        # print('  {}'.format(_brand))
                        build_trivial_index(connection, [_brand])

            print('Обработка брендов завершена')

        except ConnectionFailure as error:
            print("Не удалось подключиться к базе данных: {}".format(error))

        exit(0)

    # Обрабатываем только указанные бренды
    brands = [b.strip() for b in args.brands.split(',')]

    try:

        # Подключаемся к базе данных и строим индексы только для указанных брендов
        connection = MongoClient(uri)
        build_trivial_index(connection, brands)

    except ConnectionFailure as error:
        print("Не удалось подключиться к базе данных: {}".format(error))
