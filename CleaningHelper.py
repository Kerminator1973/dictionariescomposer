#!/usr/bin/python
# -*- coding: utf-8

from collections import OrderedDict
from operator import itemgetter


# Функция удаляет из названия товара бренд, потенциальный тип товара и вариации
def remove_brand_and_product_type(_name, _brand, _product_types):
    """
    :param str _name: полное название товара
    :param str _brand: бренд
    :param list _product_types: список типов товаров, характерных для конкретной товарной группы
    :return str
    """

    _upper_brand = _brand.upper()
    _upper_name = _name.upper()

    # Ищем совпадение типа товара по списку типов конкретной товарной группы
    for _product_type in _product_types:

        pos = _upper_name.find(_product_type)
        if pos >= 0:
            _upper_name = _upper_name[pos + len(_product_type):]
            break

    # Удаляем из названия товара бренд
    pos = _upper_name.find(_upper_brand)
    if pos >= 0:
        _upper_name = _upper_name[pos + len(_upper_brand):]

    # Удаляем из названия товара дополнительные атрибуты, если они идёт после запятой
    pos = _upper_name.find(',')
    if pos >= 0:
        _upper_name = _upper_name[:pos]

    # Так же удаляем текст в скобках. Такой текст может содержать
    # вариации, которые представляются отдельными полями.
    # Цель состоит в том, чтобы превратить:
    # "Подгузники Pampers Premium Care 4 (8-14 кг) 104 шт.
    # в "Premium Care 4 104 шт.". См. DictionaryComposer #7
    _begin_pos = _upper_name.find('(')
    _end_pos = _upper_name.find(')')

    if 0 <= _begin_pos < _end_pos:

        first_part = _upper_name[:_begin_pos].strip()
        third_part = _upper_name[_end_pos + 1:].strip()

        _upper_name = first_part + ' ' + third_part

    return _upper_name


# Функция удаляет из названия товара только бренд и текст перед ним
# ToDo: уже не используется
def remove_brand(_name, _brand):
    """
    :param str _name: полное название товара
    :param str _brand: бренд
    :return str
    """

    _upper_brand = _brand.upper()
    _upper_name = _name.upper()

    # Удаляем из названия товара бренд
    pos = _upper_name.find(_upper_brand)
    if pos >= 0:
        _upper_name = _upper_name[pos + len(_upper_brand):]

    return _upper_name


# Функция осуществляет поиск типа товара по "хлебным крошкам"
def find_potential_product_type(_name, _breadcrumbs):
    """
    :param str _name: полное название товара
    :param str _breadcrumbs: хлебные крошки товара в формате: КАТЕГОРИЯ/ПОДКАТЕГОРИЯ/.../ТОРГОВАЯ МАРКА
    :return str
    """

    _upper_name = _name.upper()
    _upper_breadcrumbs = _breadcrumbs.upper()

    type_variants = _upper_breadcrumbs.split('/')
    for variant in reversed(type_variants):

        _variant = variant.strip()

        pos = _upper_name.find(_variant)
        if pos >= 0:
            return _variant

    return None


# Функция выделяет настоящее имя бренда. Довольно часто, бренд указываемые
# в качестве такого в свойствах товара, импортированный из внешних баз данных,
# является не точным. Например, есть "Mattel Games Uno". Часто в качестве бренда
# указывается "Mattel", но на самом деле - это "Mattel Games", а Uno - название игры
def find_real_brand(product_name, brands_list, specified_brand):
    """
    :param str product_name: полное название товара
    :param list brands_list: список вероятных брендов товара
    :param str specified_brand: бренд, указанный в базе данных
    :return str
    """

    _brands = []

    for element in brands_list:
        _brands.append(element.upper())

    if specified_brand is not None:
        _specified_brand = specified_brand.upper()

        if _specified_brand not in _brands:
            _brands.append(_specified_brand)

    # Сортируем список вероятных брендов по длине. Короткие строки находятся в конце списка.
    _brands.sort(key=len, reverse=True)

    for _candidate in _brands:

        pos = product_name.find(_candidate)
        if pos >= 0:
            return _candidate

    return None


def remove_elements_by_threshold(calculated_entries, divisor=20, threshold=0,
                                 remove_simplest=True, detailed_log=False):
    """
    :param dict calculated_entries: ключ - "текстовая строка", значение - количество упоминаний
    :param remove_simplest: если есть "HOT WHEELS" и "HOT WHEELS BIG FOOT" оставить более длинный
    :param int divisor: делитель порога отсечения. Удаляется всё, что встречается в divisor раз реже, чем максимум
    :param int threshold: пороговое значение для отсечения. Имеет приоритет над divisor
    :param detailed_log: флаг с указанием необходимости вывода детальной информации
    :return list
    """

    ordered_entries = OrderedDict(sorted(calculated_entries.items(), key=itemgetter(1), reverse=True))

    # Удаляем из списка варианты, в которых общее количество товаров меньше 5% от максимального значения
    # (или 100% / divisor). Но если явно указан порог (threshold), то divisor игнорируется

    if detailed_log:
        print('Полный список кандидатов на названия моделей...')

    for key in ordered_entries:

        if detailed_log:
            print(' {}: {}'.format(key, ordered_entries[key]))

        if threshold == 0:
            threshold = ordered_entries[key] / divisor

            # Порог является вещественным числом и может быть меньше единицы,
            # что может сломать всю суть алгоритма - отсечь наименее популярные
            # варианты, которые, скорее всего, являются информационным шумом
            if threshold < 1:
                threshold = 1

            if detailed_log:
                print('Порог отсечения: {}'.format(threshold))

        # Выбрасываем из не отсортированного списка значения, которые не проходят порог
        if ordered_entries[key] < threshold:
            calculated_entries.pop(key, None)

    variants = list(calculated_entries.keys())

    if not remove_simplest:
        return variants

    # В результирующий список добавляем варианты, только в том случае, если нет
    # другого варианта, который содержит подстроку с текущим вариантом. Другими
    # словами, если в списке вариантов есть "Кукла Эмма" и "Кукла", то следует
    # сохранить вариант "Кукла Эмма", чтобы не потерять полезную информацию
    results = []
    for model_one in variants:

        add_model = True
        for model_two in variants:
            if model_one != model_two:
                if len(model_one) < len(model_two):
                    if model_one == model_two[:len(model_one)]:
                        add_model = False
                        break

        if add_model:
            results.append(model_one)

    return results

