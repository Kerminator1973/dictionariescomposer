#!/usr/bin/python
# -*- coding: utf-8

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
import operator
import json
import io                           # Библиотека для чтения/записи данных в файл
import codecs                       # Библиотека используемые при импорта JSON-словарей
from transliterate import translit  # Установка библиотеки: pip3 install transliterate
import string                       # Package включает различные таблицы символов
import collections                  # Package содержащий дополнительные коллекции, например, defaultdict
import unicodedata                  # Package для нормализации Unicode-строк (например, удаления accents)
import html                         # Специализированные утилиты для перекодировки html-данных


# Вспомогательный класс Encoder, позволяющий экспортировать объекты в JSON
class BrandsEncoder(json.JSONEncoder):
    def default(self, obj):
        return json.JSONEncoder.default(self, obj)


def print_brands_statistics(db_connection):

    # Рассматриваем только находящиеся в продаже товаре, считая информацию о них
    # актуальной. Устаревшие данные могут содержать искажения
    playsets_collection = db_connection.scrape.playsets.find({"instock": {"$eq": True}, "productXML": {"$ne": None}})

    products_count = playsets_collection.count()
    print('Общее количество записей в коллекции товаров: {}'.format(products_count))

    if products_count > 0:
        # Получаем количество записей в коллекции, в которых установлен бренд
        playsets_with_brands = connection.scrape.playsets.find({"brand": {"$exists": True},
                                                                "instock": {"$eq": True},
                                                                "productXML": {"$ne": None}})
        _count = playsets_with_brands.count()
        _percent = round((_count * 100) / products_count, 2)
        print('Товаров с выделенным брендом {}: {}%'.format(_count, _percent))


def print_unrecognisable_brands(db_connection):

    complete_brands_list = json.load(codecs.open('dirty_brands.json', 'r', 'utf-8-sig'))

    # Рассматриваем только находящиеся в продаже товаре, считая информацию о них
    # актуальной. Устаревшие данные могут содержать искажения

    playsets = db_connection.scrape.playsets.aggregate([
        {"$group": {"_id": "$brand", "count": {"$sum": 1}}},
        {"$sort": {"count": -1}}])

    unique_pairs = []
    for element in playsets:

        if element.get('_id') is None:
            continue

        _current_brand = element['_id'].upper()

        for _b in complete_brands_list:

            _brand = _b.upper()

            if _current_brand.find(_brand) == 0:
                if len(_current_brand) > len(_brand):
                    if _current_brand[len(_brand)] == ' ':

                        pair = '{} -> {} => {}'.format(_current_brand, _brand, _current_brand[len(_brand) + 1:])
                        if pair not in unique_pairs:
                            unique_pairs.append(pair)

    for element in unique_pairs:
        print(element)

    print('Завершено')


def print_brands_failures(db_connection):

    companies_brands = json.load(codecs.open('Brands.json', 'r', 'utf-8-sig'))
    if companies_brands is None:
        print('Не удалось загрузить справочник брендов')

        # Выводим на экран информацию о товарах в online-магазинах
        playsets = db_connection.scrape.playsets.find({"brand": {"$exists": True}, "instock": {"$eq": True},
                                                       "productXML": {"$ne": None}})
        total_count = playsets.count()

        has_name_count = 0
        has_product_type_count = 0
        has_model_count = 0

        companies_to_products_dict = {}

        # Осуществляем обработку записей с целью проверки, насколько хорошо данные
        # структурированы для последующей группировки
        index = 1
        for element in playsets:

            if element.get('name') is None or element.get('brand') is None:
                continue

            has_name_count += 1

            name = element['name'].upper()
            brand = element['brand'].upper()

            pos = name.find(brand)
            if pos < 0:

                # Если из названия товара не удалось выделить бренд, пытаемся использовать
                # альтернативныые варианты написания бренда
                if brand in companies_brands:
                    for _items in companies_brands[brand]:
                        pos = name.find(_items)
                        if pos >= 0:
                            break

                if pos < 0:

                    # Если всё равно не нашли, то сохраняем все подобные случае в один словарь
                    if brand not in companies_to_products_dict:
                        companies_to_products_dict[brand] = []

                    companies_to_products_dict[brand].append(name)

                continue

            # Подсчитываем количество товаров у которых удалось выделить тип товара
            # и модель (включаю вариации)
            product_type = name[:pos].strip()
            if len(product_type) > 1:
                has_product_type_count += 1

            model = name[pos + len(brand):].strip()
            if len(model) > 1:
                has_model_count += 1

            if index % 5000 == 0:
                print('Обработано {} документов'.format(index))

            index += 1

        # Осуществляем анализ названий товаров, с целью выделения устойчивых брендов
        for company in list(companies_to_products_dict.keys()):

            words_counter = {}

            # Каждое название товара разделяем на составляющие название слова и
            # подсчитываем, сколько раз это слово встречается
            for product_name in companies_to_products_dict[company]:

                _pn = product_name.replace(',', ' ').replace('"', ' ').replace("'", ' ').\
                    replace('(', ' ').replace(')', ' ')

                product_words = _pn.split()
                for word in product_words:
                    if len(word) == 0:
                        continue

                    if word not in words_counter:
                        words_counter[word] = 1
                    else:
                        words_counter[word] += 1

            # Предполагаем, что не более 20% слов, которые встречались нам в названии,
            # являются брендом
            sorted_words = sorted(words_counter.items(), key=operator.itemgetter(1), reverse=True)
            count_to_keep = int(len(sorted_words) / 6) + 1
            possible_brand = sorted_words[0:count_to_keep]

            # Алгоритм автоматического выделения названий брендов может быть крайне комплексным
            # и пока мы только выделяем общий бренды с примерами названий, чтобы оператор мог
            # выделить наиболее вероятных кандидатов в бренды
            print('Производитель: "{}". Примеры для поиска брендов:'.format(company))

            for element in possible_brand:

                example = None

                for product_name in companies_to_products_dict[company]:
                    if product_name.find(element[0]) >= 0:
                        example = product_name
                        break

                if example is not None:
                    print('  - {}'.format(example))

        # Выводим информацию о количестве товаров с названием name
        percent = round((has_name_count * 100) / total_count, 2)
        print('Товаров с название в XML-структуре {}: {}%'.format(has_name_count, percent))

        # Выводим информацию о количестве успешно выделенных типов товара и моделей
        percent = round((has_product_type_count * 100) / total_count, 2)
        print('Товаров с выделенным типом товара {}: {}%'.format(has_product_type_count, percent))

        percent = round((has_model_count * 100) / total_count, 2)
        print('Товаров с выделенной моделью {}: {}%'.format(has_model_count, percent))


def save_all_brands_to_file(db_connection):

    playsets = db_connection.scrape.playsets.find({"brand": {"$exists": True}, "instock": {"$eq": True},
                                                   "productXML": {"$ne": None}})
    all_brands = []

    # Собираем в отдельную коллекцию все доступные бренды
    index = 1
    for element in playsets:

        # Отображаем счётчик обработанных данных для того, чтобы видеть прогресс
        # в скачивании данных из базы
        if index % 5000 == 0:
            print('Обработано {} документов'.format(index))

        index += 1

        if element.get('name') is None or element.get('brand') is None:
            continue

        brand = element['brand'].upper()

        if brand in all_brands:
            continue

        all_brands.append(brand)

    print('Общее количество брендов: {}'.format(len(all_brands)))

    # Сохраняем список брендов в текстовый файл
    with io.open('brands.txt', 'w', encoding='utf8') as file:
        for brand in all_brands:
            file.write(brand + '\n')


def remove_accents(data):
    return ''.join(x for x in unicodedata.normalize('NFKD', data) if x in string.ascii_letters)


# Функция удаляет из сущности, обозначенной, как бренд ту часть, которая
# может являться товарным знаком, принадлежащим другой компании
def remove_suspicious_trademark(_current_brand, complete_brands_list):

    for _b in complete_brands_list:

        if _current_brand.find(_b) == 0:
            if len(_current_brand) > len(_b):
                if _current_brand[len(_b)] == ' ':
                    return _b

    return _current_brand


# Функция подсчитывает количество упоминаний брендов в базе данных,
# ищет синонимы названий брендов и строит список брендов для группировки
# товаров в отдельных брендах
def find_brand_synonyms(db_connection):

    # Загружаем автоматически сформированный, полный список брендов товаров
    complete_brands_list = json.load(codecs.open('dirty_brands.json', 'r', 'utf-8-sig'))

    # Применяем агрегацию для выделения уникальных названий брендов
    # и подсчёта количества их использования по всем товарам
    playsets = db_connection.scrape.playsets.aggregate([
        {"$group": {"_id": "$brand", "count": {"$sum": 1}}},
        {"$sort": {"count": -1}}])

    records_count = 0

    brand_count = {}
    all_brands = []
    for brand in playsets:

        records_count += 1

        if brand.get('_id') is None:
            continue

        # Игнорируем бренды, с которыми связано 5, или меньше товаров
        if brand.get('count') is None or brand['count'] <= 5:
            continue

        if len(brand['_id']) > 0:
            # Преобразовываем бренд к верхнему регистру и удаляем специальные
            # символы, такие как амперсанд - &#039;
            _cleared_brand = html.unescape(brand['_id'].upper())
            all_brands.append(_cleared_brand)
            brand_count[_cleared_brand] = brand['count']

    print('Общее количество брендов: {}; записей: {}'.format(len(all_brands), records_count))

    ''' Тестовый набор брендов, для отладки алгоритма выделения эквивалентных названий

    all_brands.append('RÖMER')
    all_brands.append('PEG-PEREGO')
    all_brands.append('BÉABA')
    all_brands.append('PHILIPS AVENT')
    all_brands.append('AVENT')
    all_brands.append('ROEMER')
    all_brands.append('PEG PEREGO')
    '''

    all_modifications = {}

    # Таблица символов для преобразования diacritics в латинские символы
    diacritics = collections.defaultdict(lambda: None)
    diacritics.update({
        ord(u'Ä'): u'AE',
        ord(u'Ö'): u'OE',
        ord(u'Ü'): u'UE',
        ord(u'ß'): u'SS',
    })
    diacritics.update(dict(zip(map(ord, string.ascii_uppercase), string.ascii_uppercase)))
    diacritics.update(dict(zip(map(ord, string.digits), string.digits)))

    common_words = ['BABY', 'CITY', 'GAMES', 'TRAVEL', 'SMART', 'МОЙ', 'ПЛАСТИК', 'FIRST', 'BABIES']

    # Генерируем все возможные альтернативные написания бренда
    for brand in all_brands:

        all_variants = []

        # Символы пунктуации заменены на пробелы
        value = brand.replace('-', ' ')
        if value != brand:
            all_variants.append(value)

        value = brand.replace('-', '')
        if value != brand:
            all_variants.append(value)

        # Убираем символы "copyright", "registered", "trademark".
        # См.: http://www.fileformat.info/info/unicode/char/00ae/index.htm
        value = brand.replace(u"\u2122", '')
        value = value.replace(u"\u00A9", '')
        value = value.replace(u"\u00AE", '')
        value = value.replace('!', '')
        if value != brand:
            all_variants.append(value)

        # Название разделяем на отдельные составляющие, предварительно
        # убрав часть слов в конце названия, которые могут относится не
        # к бренду, в к товарным знакам. Например, если названием товара
        # является "BONDAI STAR WARS", то оставляет только "BONDAI", т.к.
        # STAR WARS - это товарный знак принадлежащий Disney, владеюший
        # компанией Lucasfilm
        if brand.find(' ') >= 0:

            _brand = remove_suspicious_trademark(brand, complete_brands_list)

            values = _brand.split(' ')
            if len(values) > 1:
                for value in values:

                    if value not in common_words:
                        all_variants.append(value.strip())

        # Генерируем вариант с удалением accents, т.е. указанием ударения
        value = remove_accents(brand)
        if value != brand:
            all_variants.append(value)

        # Генерируем варианты с подменой диакритических символов и транслитерацией
        modified_brand = brand.translate(diacritics, )

        if modified_brand == brand:

            value = translit(brand, "ru")
            if value != brand:
                all_variants.append(value)

        else:

            all_variants.append(modified_brand)

            value = translit(modified_brand, "ru")
            if value != modified_brand:
                all_variants.append(value)

        all_modifications[brand] = all_variants

    # Осуществляем сравнение всех брендов со всеми, с целью нахождения
    # пересечений в производных (сгенерированных) названиях

    brands_clusters = []

    for brand in all_brands:

        for key in list(all_modifications.keys()):

            if key == brand:
                continue

            for variant in all_modifications[key]:

                if variant == brand:

                    # Найдена пара брендов синонимов. Теперь их нужно поместить
                    # в кластер синонимов. Пар с совпадениями названий может быть
                    # достаточно много и имеет смысл группировать все подобные
                    # пересечения в одном массиве
                    is_found = False

                    for cluster in brands_clusters:

                        if brand in cluster or key in cluster:

                            is_found = True

                            if brand not in cluster:
                                cluster.append(brand)

                            if key not in cluster:
                                cluster.append(key)

                            break

                    if not is_found:
                        new_elements = [brand, key]
                        brands_clusters.append(new_elements)

    # Возвращаем полный список брендов, словарь количества упоминаний брендов
    # в описании товаров и списки брендов-синонимов
    return all_brands, brand_count, brands_clusters


def print_different_names_of_brands(db_connection):

    all_brands, brand_count, brands_clusters = find_brand_synonyms(db_connection)

    # После того, как все кластеры эквивалентных брендов сформированы,
    # выводим все эквиваленты на экран
    #
    # Параллельно генерируем Python-объекты и экспортируем их в виде JSON-структур

    print('Список эквивалентных названий брендов ...')

    obj_to_export = {}  # Словарь "Главный бренд" + "синонимы"
    substitutions_to_export = {}
    for elements in brands_clusters:

        _max_count = 0
        _brand = ''
        for brand in elements:
            if brand_count[brand] > _max_count:
                _max_count = brand_count[brand]
                _brand = brand

        str_equal_brands = ['{} ({})'.format(x, brand_count[x]) for x in elements]
        print('"{}" ({}): {}'.format(_brand, _max_count, str_equal_brands))

        equals_to_export = []

        for brand in elements:
            if brand != _brand:
                equals_to_export.append(brand)

        obj_to_export[_brand] = equals_to_export

        for synonym in equals_to_export:
            substitutions_to_export[synonym] = _brand

    # Сохраняем накопленные данные в виде JSON-объекта
    with open('brand_equals.json', 'w', encoding='utf8') as outfile:
        json.dump(obj_to_export, outfile, cls=BrandsEncoder, ensure_ascii=False, sort_keys=True)

    with open('brand_synonyms.json', 'w', encoding='utf8') as outfile:
        json.dump(substitutions_to_export, outfile, cls=BrandsEncoder, ensure_ascii=False, sort_keys=True)


if __name__ == "__main__":

    # Подключаемся к базе данных, хранящей результаты обработки XML-данных
    try:

        # Для подключения к локальной базе данных, можно не указывать
        # параметры подключения
        # client = MongoClient()
        # cursor = client.online_shopping_scrapers.detmir_brands.find()

        uri = 'mongodb://authorizedScrape:authorizedUser$456@77.244.213.247:27017/scrape'
        connection = MongoClient(uri)

        # При активации следующей ниже строки печатается информация о фактической
        # доступности поля бренд в описаниях товара
        # print_brands_statistics(connection)

        # При активации следующей ниже строки печатается информация ооб ошибках
        # в названии бренда, в строке описания товара
        # print_brands_failures(connection)

        # Функция сохраняет все выделенные бренды в отдельный файл
        # save_all_brands_to_file(connection)

        # print_unrecognisable_brands(connection)

        # Функция печатает список брендов, которые могут быть записаны разными способами
        print_different_names_of_brands(connection)

    except ConnectionFailure as dberror:
        print("Не удалось подключиться к базе данных: {}".format(dberror))
