#!/usr/bin/python
# -*- coding: utf-8

import unicodedata
import string
from CleaningHelper import find_real_brand


# Класс осуществляет разделение строки с описанием товара на
# отдельные составляющие с определением их вероятного типа
class ProductTokenizer:

    # Типы токенов, распознаваемые данным классом
    TOKEN_UNKNOWN = 0
    TOKEN_PRODUCT_TYPE = 1
    TOKEN_BRAND = 2
    TOKEN_MODEL = 3
    TOKEN_SET_NAME = 4
    TOKEN_VARIATION = 5
    TOKEN_AFTER_COMMA = 6
    TOKEN_ARTICLE = 7
    TOKEN_PROPERTY = 8
    TOKEN_NEED_TO_BE_PROCESSED = 0

    @staticmethod
    def token_repr(token_type):
        return {
            ProductTokenizer.TOKEN_PRODUCT_TYPE: 'Product Type',
            ProductTokenizer.TOKEN_BRAND: 'Brand',
            ProductTokenizer.TOKEN_MODEL: 'Model',
            ProductTokenizer.TOKEN_SET_NAME: 'Set\'s name',
            ProductTokenizer.TOKEN_VARIATION: 'Variation',
            ProductTokenizer.TOKEN_AFTER_COMMA: 'After Comma',
            ProductTokenizer.TOKEN_ARTICLE: 'Article',
            ProductTokenizer.TOKEN_PROPERTY: 'Property',
            ProductTokenizer.TOKEN_NEED_TO_BE_PROCESSED: 'Need to be processed'
        }.get(token_type, 'Unknown')

    def __init__(self, _brands_list):
        self.brands_list = _brands_list
        self.product_name = None
        self.original_name = None

        # Кандидаты на добавление в справочники
        self.product_type = None
        self.trademark_candidate = None

        # Идентификатор записи в базе данных (для обновления записи о товаре)
        self.mongo_id = None

        # Контейнер токенов
        self.tokens = []

    # Функция добавляет очередной токен в массив токенов
    def _add_token(self, _text, _token_type):
        if len(_text) > 0:

            _text = _text.lstrip(string.punctuation).rstrip(string.punctuation).strip()
            self.tokens.append((_text, _token_type))

    def tokenize_trivial(self, element):

        # Сохраняем оригинальное название товара и идентификатор записи в базе данных
        self.mongo_id = element['_id']
        self.original_name = element['name']

        # Обрабатывать название товара мы будем используя значение поля 'name'
        self.product_name = element['name'].upper()

        # Удаляем управляющие коды таблицы Unicode, в частности [APC]
        self.product_name = "".join(ch for ch in self.product_name if unicodedata.category(ch)[0] != "C")

        # Удаляем избыточные символы
        self._clean()

        # Удаляем пробелы между амперсандом и другими словами -
        # это позволяет корректно сравнивать такие токены, как, например:
        # "Thomas&Friends" и "Thomas & Friends". Так же заменяем кривую кавычку
        # на одинарную - это позволяет решить проблему правильного выделения
        # торгового знака "MY MINI MIXIEQ’S"
        self.product_name = self.product_name.replace(' &', '&').replace('& ', '&').replace('’', "'")

        # Выполняем различные процедуры обработки названия товара
        self._identify_product_type(element)        # попытка определить тип товара
        self._process_brand(element)
        self._process_parentheses()                 # ...текст в круглых скобках
        self._process_commas()                      # ...текст после запятых
        self._process_quotes()                      # ...текст в кавычках
        self._find_articles()                       # ...комбинации типа K0888, или CJT18
        self._process_specified_features(element)   # явно указанные свойства (в базе данных)

        # Удаляем знаки пунктуации в оставшейся части названия товара
        if len(self.product_name) > 0:
            self.product_name = self.product_name.lstrip(string.punctuation).rstrip(string.punctuation).strip()

        # Если осталось что-то не распознанное, обработаем это
        # на втором этапе, после того, как будет собраны справочники
        pass

    # Функция осуществляет попытку обработки оставшегося текста, с учётом
    # сформированных справочников
    def correct_by_dictionaries(self, product_types, models, trademarks):

        # По предоставленным справочникам, уточняем названия типов товаров,
        # моделей и товарных знаков/вариаций
        if len(self.product_name) > 0:

            for _item in product_types:
                self._try_specify(_item, ProductTokenizer.TOKEN_PRODUCT_TYPE)

            for _item in models:
                self._try_specify(_item, ProductTokenizer.TOKEN_MODEL)

            for _item in trademarks:
                self._try_specify(_item, ProductTokenizer.TOKEN_AFTER_COMMA)

    # Функция осуществляет коррекцию типов токенов, исходя из их структуры,
    # а также заменяет синонимы
    def finalize(self, model_synonyms, variation_synonyms):
        """
        :param dict model_synonyms: синонимы моделей
        :param dict variation_synonyms: синонимы вариаций и товарных знаков
        :return int: количество замен синонимов
        """

        # Выполняем коррекцию типов токенов
        self._clarify_token_type()
        self._clarify_model()

        if len(self.product_name) > 0:

            # Используется какая-то другая схема, фиксируем не распознанный остаток
            # как "что-то ещё"
            self._add_token(self.product_name, ProductTokenizer.TOKEN_NEED_TO_BE_PROCESSED)

        # Выполняем замену синонимов
        _count = 0
        updated_list = []
        for _t in self.tokens:

            if _t[0] in model_synonyms:
                _count += 1
                updated_list.append((model_synonyms[_t[0]], _t[1]))

            elif _t[0] in variation_synonyms:
                _count += 1
                updated_list.append((variation_synonyms[_t[0]], _t[1]))

            else:
                updated_list.append((_t[0], _t[1]))

        # Если были какие-то подмены, то заменяем список токенов на обновлённый
        if _count > 0:
            self.tokens = updated_list

        # Отдельно обрабатываем выражение "В АССОРТИМЕНТЕ"
        self._in_assortment()

        return _count

    # Функция уточняет типы токенов, используя эвристические схемы
    def _clarify_token_type(self):

        if len(self.product_name) > 0:

            # Схема: Brand + Product Type + [Что-то ещё] = Brand + Product Type + Model
            if len(self.tokens) == 2:

                if self.tokens[0][1] == ProductTokenizer.TOKEN_BRAND:
                    if self.tokens[1][1] == ProductTokenizer.TOKEN_PRODUCT_TYPE:

                        self._add_token(self.product_name, ProductTokenizer.TOKEN_MODEL)
                        self.product_name = ''
                        return

                if self.tokens[0][1] == ProductTokenizer.TOKEN_PRODUCT_TYPE:
                    if self.tokens[1][1] == ProductTokenizer.TOKEN_BRAND:
                        self._add_token(self.product_name, ProductTokenizer.TOKEN_MODEL)
                        self.product_name = ''
                        return

    # Функция уточняет название модели, используя эвристические схемы
    def _clarify_model(self):

        # Устраняем дублирование названия моделей, если несколько
        # моделей являются схожими по написанию.
        # Пример дублирования, которое нужно устранить: UNO и УНО
        for _t in self.tokens:
            # TODO: Ещё не реализовано
            pass

        # Проверяем, а была ли в названии товара найдена модель? Если
        # модель не была найдена, но остался какой-то не распознанный
        # кусок текста, то считаем, что этот кусок текста и является
        # моделью
        if len(self.product_name) > 0:

            _models_count = 0
            for _t in self.tokens:
                if _t[1] == ProductTokenizer.TOKEN_MODEL:
                    _models_count += 1

            if _models_count == 0:
                self._add_token(self.product_name, ProductTokenizer.TOKEN_MODEL)
                self.product_name = ''

    # Функция осуществляет проверку наличия среди токенов слов "В АССОРТИМЕНТЕ".
    # Наличие такого слова означает, что модель отсутствует, а токен, в котором
    # встречается такое слово, нужно разделять на отдельные токены, типом которого
    # является ProductTokenizer.TOKEN_PROPERTY (свойство, но не вариация)
    def _in_assortment(self):

        _marker = "В АССОРТИМЕНТЕ"
        _whole_word_markers = ["В АСС.", "В АСС"]

        # Выполняем замену синонимов
        has_assortment = False
        for _t in self.tokens:
            if _t[0].find(_marker) >= 0 or _t[0] in _whole_word_markers:
                has_assortment = True
                break

        if has_assortment:

            updated_list = []
            for _t in self.tokens:

                # Пример, которые обрабатывает это условие: "МАШИНКА HOT WHEELS, В АСС"
                if _t[0] in _whole_word_markers:
                    updated_list.append((_marker, ProductTokenizer.TOKEN_PROPERTY))
                    continue

                # Пример, которые обрабатывает это условие: "1:55 В АССОРТИМЕНТЕ"
                pos = _t[0].find(_marker)
                if pos >= 0:

                    if pos > 0:
                        updated_list.append((_t[0][:pos].strip(), ProductTokenizer.TOKEN_PROPERTY))

                    right_part = _t[0][pos + len(_marker):]
                    if len(right_part) > 0:
                        updated_list.append((right_part.strip(), ProductTokenizer.TOKEN_PROPERTY))

                    updated_list.append((_marker, ProductTokenizer.TOKEN_PROPERTY))

                else:
                    updated_list.append((_t[0], _t[1]))

            self.tokens = updated_list

    # Функция возвращает тривиальный индекс товара, который состоит из
    # типа товара и модели
    def get_trivial_index(self, popular_after_commas):
        """
        :param list popular_after_commas: список наиболее популярных слов после запятых
        """

        _product_type = ''
        _model = ''
        _after_commas = []

        for _t in self.tokens:

            if _t[1] == ProductTokenizer.TOKEN_PRODUCT_TYPE:
                _product_type = _t[0]
                continue

            if _t[1] == ProductTokenizer.TOKEN_MODEL:
                _model = _t[0]
                continue

            if _t[1] == ProductTokenizer.TOKEN_AFTER_COMMA:
                _after_commas.append(_t[0])
                continue

        if len(_product_type) == 0 or len(_model) == 0:

            # Если у товара отсутствует тип товара, то пытаемся создать
            # индекс из модели и слов, идущих после запятой
            if len(_model) != 0 and len(_after_commas) > 0:

                # Выбираем наиболее популярный "after commas" из списка
                the_most_popular = _after_commas[0]
                the_best_index = 99999

                for _a in _after_commas:
                    for _i, _p in enumerate(popular_after_commas):
                        if _a == _p:
                            if _i < the_best_index:
                                the_most_popular = _a
                                the_best_index = _i
                            break

                # Заменяем тип товара текстом, который идёт после запятой,
                # т.к. это может привести к качественной классификации,
                # в таких случаях, как:
                #   Набор кубиков, Fisher-Price
                #   Кукла-невеста Barbie
                _product_type = the_most_popular

            else:

                # Если сформировать индекс не удалось, то используем все токены,
                # за исключением бренда и артикула. Артикул не используем, т.к.
                # во многих названиях товара он не используется и это приводит к
                # ухудшению
                _words = []
                for _t in self.tokens:
                    if _t[1] != ProductTokenizer.TOKEN_BRAND and _t[1] != ProductTokenizer.TOKEN_ARTICLE:
                        _words.extend(_t[0].split(' '))

                return self._unique_and_sort(_words)

        # Если были обнаружены тип товара и модель, то их более, чем
        # достаточно для качественной классификации товаров
        _model_words = _model.split(' ')
        _words = _product_type.split(' ')
        _words.extend(_model_words)

        return self._unique_and_sort(_words)

    # Вспомогательный метод, осуществляющий устранение дубликатов,
    # а затем сортировку слов по алфавиту, с целью создания trivial index
    def _unique_and_sort(self, _words):

        # Индекс начинаем с самого популярного написания бренда. Например,
        # если бренд указан как "КАКАДУ" и "KAKADU", для всех товаров будет
        # использован префикс "КАКАДУ_"
        brand_prefix = ''
        if len(self.brands_list) > 0:
            brand_prefix = self.brands_list[0].strip().replace(' ', '_') + '_'

        _words = list(set(_words))
        _words.sort(reverse=False)
        return brand_prefix + '-'.join(_words)

    # Функция осуществляет попытку выделить единственное число товара по
    # указанному множественному числу, описанному в свойствах 'producttype'.
    # Задача состоит в том, чтобы понять, что тип товара "МАШИНКИ" и слово
    # "МАШИНКА" в названии товара - это одно и тоже
    def _identify_product_type(self, element):

        if element.get('breadcrumb') is None:
            return

        _breadcrumb = element['breadcrumb'].strip().split('/')
        if len(_breadcrumb) == 0:
            return

        # Отбираем два последних предложения из breadcrumb, сортируем
        # их по длине (уменьшая) - чтобы избежать ложного выделения
        # слова "ИГРА", тогда как в названии товара "НАСТОЛЬНАЯ ИГРА".
        # Дублируем кандидатов, добавляя в начале слово "НАБОР", чтобы
        # выделить "НАБОР ФИГУРОК", а не "ФИГУРОК".
        # Слово "НАБОР" - особенное и универсальное, определяет некоторое
        # количество элементов
        _type_candidates = _breadcrumb[-2:]

        # Встречаются и вот такие варианты: "ТРАНСПОРТ, МАШИНКИ". В подобных
        # случаях, разделяем вариант на составляющие слова
        has_comma = False
        for _t in _type_candidates:
            if _t.find(',') >= 0:
                has_comma = True
                break

        if has_comma:
            splitted = []
            for _t in _type_candidates:
                splitted.extend([x.strip() for x in _t.split(',')])
            _type_candidates = splitted

        # Сортируем варианты по количеству букв, с тем, чтобы сначала пытаться
        # выделить на наиболее точные типы товаров, а потом уже более простые
        _type_candidates.sort(key=len, reverse=True)

        _extended_candidates = []
        for _t in _type_candidates:
            _extended_candidates.append("НАБОР " + _t)
            _extended_candidates.append(_t)

        # ДОПОЛНИТЕЛЬНАЯ ТРАССИРОВКА:
        # print(' # {} => {} '.format(_type_candidates, self.product_name))

        _product_words = self.product_name.split(' ')
        for _candidate in _extended_candidates:

            _type_words = _candidate.strip().upper().split(' ')

            # Если в названии модели слов больше, чем в названии продукта,
            # ясно что совпадений не будет
            steps = len(_product_words) - len(_type_words)
            if steps >= 0:

                # Проверяем на совпадение каждую группу слов в названии продукта,
                # которые могут быть похожи на название товара
                for _n in range(0, steps + 1):

                    _words_to_compare = _product_words[_n:_n + len(_type_words)]
                    if self._is_lists_equal_exclude_endings(_type_words, _words_to_compare):
                        joined_type = ' '.join(_words_to_compare)
                        self.product_type = joined_type.lstrip(string.punctuation).rstrip(string.punctuation).strip()

                        # Если был обнаружен тип товара и он находится после какого-то
                        # текста, то предполагаем, что этот текст - кандидат в товарные знаки
                        pos = self.product_name.find(self.product_type)
                        if pos > 0:
                            self.trademark_candidate = self.product_name[:pos].strip()

                        # ДОПОЛНИТЕЛЬНАЯ ТРАССИРОВКА:
                        # print(' $ {}'.format(joined_type))
                        return

    # Функция сравнивает два списка слов на соответствие слов, без
    # учёта окончания.
    # Примеры списков для сравнения: "ИГРОВОЙ НАБОР" и "ИГРОВЫЕ НАБОРЫ"
    @staticmethod
    def _is_words_equal_exclude_endings(_first, _second):

        _count = 0
        for _n, _char in enumerate(_first):

            # Вторая строка закончилась - соответственно все символы,
            # которые остались в первой строке добавляются к числу различий
            if len(_second) <= _n:
                _count += len(_first) - len(_second)
                break

            if _char != _second[_n]:
                _count += 1

        if _count > 2:
            return False

        # Если отличных символов не больше двух и общее количество отличных
        # символов не превышает 50%, то считаем слова похожими
        difference = _count / max(len(_first), len(_second))
        return difference < 0.5

    def _is_lists_equal_exclude_endings(self, _first, _second):

        for _n, _word_in_first in enumerate(_first):

            _word_in_second = _second[_n]
            equal_word = self._is_words_equal_exclude_endings(_word_in_first, _word_in_second)
            if not equal_word:
                return False

        return True

    # Функция выделяет название бренда из названия товара
    def _process_brand(self, element):

        _brand = find_real_brand(self.product_name, self.brands_list, element.get('brand'))
        if _brand is None:

            # Если в названии товара не удалось найти бренд, то
            # если он указан среди свойств товаров, то принимаем его.
            #
            # Бренд обязательно должен присутствовать, т.к. по нему мы делаем
            # выборку товаров из базы данных
            _brand = element['brand'].upper()

        else:

            # Позиция бренда очень важна. Если бренд находится в конце текста, то
            # остальная часть - это название товара. Если бренд находится в середине,
            # то что в начале может быть типом товара

            pos = self.product_name.find(_brand)
            _text_before = self.product_name[:pos].strip()
            _text_after = self.product_name[pos + len(_brand):].strip()

            if _text_after is None or len(_text_after) == 0:

                self.product_name = _text_before

            else:

                # Предполагаем, что текст до бренда - это тип товара. Вместе
                # с тем, маловероятно, что тип товара будет состоять более,
                # чем из двух слов
                _potentially_type = _text_before.split(' ')
                if len(_potentially_type) > 2:

                    self.product_name = _text_before + ' ' + _text_after

                else:

                    self.product_name = _text_after
                    self._add_token(_text_before, ProductTokenizer.TOKEN_PRODUCT_TYPE)

        self._add_token(_brand, ProductTokenizer.TOKEN_BRAND)

    # Функция выделяет в виде отдельных токенов названия вариаций,
    # или товарных знаков
    def _process_commas(self):

        _candidates = self.product_name.split(',')

        _product_name = ''
        for index, text in enumerate(_candidates):
            if index == 0:
                _product_name = text.strip()
                continue

            self._add_token(text.strip(), ProductTokenizer.TOKEN_AFTER_COMMA)

        self.product_name = _product_name

    # Функция выделяет в виде отдельных токенов названия моделей,
    # либо наборов
    def _process_quotes(self):

        self.product_name = self.product_name.replace('«', '"')
        self.product_name = self.product_name.replace('»', '"')

        # Если кавычек в названии товара нет, то не обрабатываем документ
        position = self.product_name.find('"')
        if position < 0:
            return

        _candidates = self.product_name.split('"')

        length = len(_candidates)
        if length == 1:

            self.product_name = _candidates[0]

        elif length == 2:

            if position == 0:

                # Если текст в кавычках находится в самом начале строки, то считаем,
                # что в кавычках указывается названия модели, а после - вариация
                self._add_token(_candidates[0].strip(), ProductTokenizer.TOKEN_MODEL)
                self.product_name = _candidates[1].strip()

            else:

                self.product_name = _candidates[0].strip()
                self._add_token(_candidates[1].strip(), ProductTokenizer.TOKEN_MODEL)

        elif length == 3:

            if position == 0:

                # Если текст в кавычках находится в самом начале строки, то считаем,
                # что в кавычках указывается названия модели, а второй текст в кавычках -
                # это серия товара
                self._add_token(_candidates[0].strip(), ProductTokenizer.TOKEN_MODEL)
                self.product_name = _candidates[1].strip()
                self._add_token(_candidates[2].strip(), ProductTokenizer.TOKEN_SET_NAME)

            else:

                self.product_name = _candidates[0].strip()
                self._add_token(_candidates[1].strip(), ProductTokenizer.TOKEN_MODEL)
                self._add_token(_candidates[2].strip(), ProductTokenizer.TOKEN_VARIATION)

        elif length == 4:

            if position == 0:

                self._add_token(_candidates[0].strip(), ProductTokenizer.TOKEN_MODEL)
                self._add_token(_candidates[1].strip(), ProductTokenizer.TOKEN_VARIATION)
                self._add_token(_candidates[2].strip(), ProductTokenizer.TOKEN_SET_NAME)
                self._add_token(_candidates[2].strip(), ProductTokenizer.TOKEN_VARIATION)

            else:

                self.product_name = _candidates[0].strip()
                self._add_token(_candidates[1].strip(), ProductTokenizer.TOKEN_MODEL)
                self._add_token(_candidates[2].strip(), ProductTokenizer.TOKEN_VARIATION)
                self._add_token(_candidates[2].strip(), ProductTokenizer.TOKEN_SET_NAME)
        else:

            # Если текст в кавычках встречается чаще, чем два раза,
            # то это особый случай, который нужно будет рассматривать
            # по отдельной схеме
            self.product_name = self.product_name.replace('"', '')

    # Функция выделяет текст в круглых скобках, как вариацию
    def _process_parentheses(self):

        while True:

            pos_left = self.product_name.find('(')
            pos_right = self.product_name.find(')')
            if pos_left < 0 or pos_right < 0 or pos_right < pos_left:
                break

            _token = self.product_name[pos_left + 1:pos_right]
            self._add_token(_token.strip(), ProductTokenizer.TOKEN_VARIATION)

            tag_before = self.product_name[:pos_left]
            tag_after = self.product_name[pos_right+1:]

            self.product_name = tag_before + ' ' + tag_after
            self._clean()

    # Функция выделяем слова являющиеся потенциальными артикулами.
    def _find_articles(self):

        _words = self.product_name.split(' ')
        for _word in _words:

            if len(_word) >= 3:

                # Артикул состоит содержит либо несколько цифр, либо цифры и буквы
                if sum(c.isdigit() for c in _word) > 0:

                    # Вместе с тем, в артикуле не должно быть символов пунктуации.
                    # Например, "3-В-1" не является артикулом товара
                    if sum(c in string.punctuation for c in _word) == 0:

                        self._add_token(_word, ProductTokenizer.TOKEN_ARTICLE)

                        # Считаем, что то, что идёт до артикула, является названием модели,
                        # но может, тем не менее, содержать ещё и товарный знак, например:
                        # "THOMAS&FRIENDS ПАРОВОЗИК ПЕРСИ W2192"
                        # Соответственно, если после артикула есть какой-то текст, то
                        # считаем, что до артикула идёт модель. Если после артикула
                        # ничего нет, то не делаем преждевременного заключения и
                        # оставляем текст для дальнейшего разбора
                        words_before = ''
                        pos = self.product_name.find(_word)
                        if pos > 0:
                            words_before = self.product_name[:pos].strip()

                        self.product_name = self.product_name[pos + len(_word):].strip()
                        if len(self.product_name) == 0:
                            self.product_name = words_before
                        else:
                            self._add_token(words_before, ProductTokenizer.TOKEN_MODEL)

                        # Считаем, что у товара может быть только один артикул
                        break

    # Функция осуществляет идентификацию элементов по отдельным
    # атрибутам товаров из базы данных
    def _process_specified_features(self, element):

        # ToDo: поле 'model' кажется бесполезным. Нужно проверить, экспериментально,
        # его эффективность. Если его нет, то не нужно на него надеятся и использовать
        # Замечание #11: это уточнение на работает
        # self._try_specify(element.get('model'), ProductTokenizer.TOKEN_MODEL)
        # self._try_specify(element.get('producttype'), ProductTokenizer.TOKEN_PRODUCT_TYPE)
        pass

    # Код осуществляет установку типа токена по его фактическому значению.
    # Вызывающий указывает, например, что словосочетание "ИГРОВОЙ НАБОР" -
    # это тип товара, а "Большой джип" - это модель
    def _try_specify(self, _specified_value, _token_type):
        """
        :param string _specified_value: искомое значение
        :param _token_type: тип токена, который нужно установить у элемента с искомым значением
        """

        if _specified_value is not None:

            _upper_value = _specified_value.strip().upper()
            pos = self.product_name.find(_upper_value)

            if pos >= 0:

                tag_after = self.product_name[pos + len(_upper_value):]

                # Если сразу за выделенным значением идёт отличный от
                # пробела символ, то не выделяем значение. Такой подход
                # позволяет, например, избежать выделения значения "ОБЕЗЬЯНКА"
                # из текста "ОБЕЗЬЯНКА-АКРОБАТ"
                if len(tag_after) > 0:
                    if tag_after[0] != ' ' and tag_after[0] != ':':
                        return

                _token = self.product_name[pos:pos + len(_upper_value)]
                self._add_token(_token.strip(), _token_type)

                tag_before = self.product_name[:pos]

                # В результате выделения товарного знака из названия товара,
                # например: "Ранец раскладной Ever After High: Dragon Game",
                # может остаться вот такая строка "Ранец раскладной : Dragon Game",
                # что является ошибкой. Удаляем символ разделитель
                if len(tag_after) > 0:
                    if tag_after[0] == ':':
                        tag_after = tag_after[1:]

                self.product_name = tag_before + ' ' + tag_after
                self._clean()

    # Функция осуществляет чистку оставшейся части названия от знаков пунктуации
    def _clean(self):

        while self.product_name.find('  ') >= 0:
            self.product_name = self.product_name.replace('  ', ' ')

        self.product_name = self.product_name.strip()

    # Функция определяет строчное представление объекта, например,
    # для использования при вызове как аргумента в print(tokenizer)
    def __repr__(self):

        representation = self.original_name + '\n'
        for _element in self.tokens:

            # Информация о бренде не является важной при анализе результатов работы
            if _element[1] == ProductTokenizer.TOKEN_BRAND:
                continue

            representation += '  "{}": {}\n'.format(_element[0], ProductTokenizer.token_repr(_element[1]))

        return representation
