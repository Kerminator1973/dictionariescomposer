#!/usr/bin/env node

// Установить библиотеку cheerio можно используя команду: npm install cheerio
const cheerio = require('cheerio')


function clear_product_name(_product_name) {

	// Заменяем ESC-символы, в частности "амперсанд" (&#039;) на обычные символы
	_product_name = cheerio.load(_product_name).text();

	// Сначала удаляем пробелы, окружающие амперсанд, чтобы сгруппировать
	// товары по модели. "SLEEP&PLAY" и "SLEEP & PLAY" это один и тот же товар
	var processed_name = _product_name.replace(" &", "&").replace("& ", "&");

	return processed_name;
}

// Выполняем проверку корректности разработанного кода
console.log("Выполняется проверка корректности кода...");

var bTest1 = clear_product_name("Johnson&#039;s Baby") == "Johnson's Baby";
console.log("Тест 1: " + (bTest1 ? "OK" : "Сбой"));

var bTest2 = clear_product_name("Pampers SLEEP & PLAY PACK") == "Pampers SLEEP&PLAY PACK";
console.log("Тест 2: " + (bTest2 ? "OK" : "Сбой"));

console.log("Проверка завершена...");
